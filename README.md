# Warning: Use at your own risk. Using a third party Discord client may result in your account being banned.

# Building from source

## Build deps

- cmake
- gcc/g++
- gtkmm4
- libadwaita
- libcurl
- libssl
- jsoncpp
- libxml2
- libsecret

## Steps
```
git clone https://gitlab.com/caveman250/clans
cd clans
mkdir bin && cd bin
cmake ..
make -j4 (try without the j argument if your pinephone runs out of ram)
```
