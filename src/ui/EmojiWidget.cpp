#include <util/ImageCache.h>
#include "EmojiWidget.h"
#include "Emojis.h"
#include "Application.h"
#include "MessageBar.h"
#include "MessageView.h"
#include "util/Helpers.h"
#include "discord/DiscordClient.h"

namespace ui
{
    EmojiWidget::EmojiWidget()
    {
        m_child = new Gtk::ScrolledWindow();
        m_child->set_parent(*this);

        auto viewport = new Gtk::Viewport(Gtk::Adjustment::create(0, 0, 0), Gtk::Adjustment::create(0, 0, 0));
        viewport->set_vscroll_policy(Gtk::Scrollable::Policy::MINIMUM);
        m_child->set_child(*viewport);

        auto box = new Gtk::Box(Gtk::Orientation::VERTICAL);
        viewport->set_child(*box);

        auto server = Application::Get()->get_discord_client()->get_server(Application::Get()->get_message_view()->get_channel().serverID);

        auto server_emoji_label = new Gtk::Label(server.name);
        server_emoji_label->set_xalign(0);
        server_emoji_label->set_margin_top(10);
        server_emoji_label->set_margin_start(10);
        server_emoji_label->set_margin_bottom(5);
        server_emoji_label->add_css_class("emoji-header");
        box->append(*server_emoji_label);

        auto server_emoji_flowbox = new Gtk::FlowBox();
        server_emoji_flowbox->set_max_children_per_line(10);
        server_emoji_flowbox->set_min_children_per_line(6);
        //server_emoji_flowbox->set_homogeneous(true);
        box->append(*server_emoji_flowbox);

        Glib::signal_timeout().connect([this, box]()
        {
            load_emojis(box);
            return m_load_index < (int)s_emojis.size();
        }, 100);

        for (const auto& server_emoji : server.emojis)
        {
            auto button = new Gtk::Button();
            button->remove_css_class("text-button");
            button->add_css_class("image-button");
            button->add_css_class("flat");
            button->signal_clicked().connect([this, server_emoji]()
            {
                m_signal_server_emoji_selected.emit(server_emoji);
            });

            server_emoji_flowbox->append(*button);

            auto picture = new Gtk::Picture();
            picture->set_size_request(16, 16);
            picture->set_can_shrink(true);
            picture->set_halign(Gtk::Align::CENTER);

            button->set_child(*picture);

            std::string url = util::Helpers::str_format("https://cdn.discordapp.com/emojis/%s.png", server_emoji.ID.string().c_str());
            //m_image_cancellation_token = new util::CancellationToken();
            util::ImageCache::get()->get_image(url, [picture](const std::string& file_path)
            {
                try
                {
                    auto texture = Gdk::Texture::create_from_file(Gio::File::create_for_path(file_path));
                    picture->set_paintable(texture);
                }
                catch (std::exception& e)
                {
                    std::cout << e.what() << std::endl;
                }
                //m_image_cancellation_token = nullptr;
            }, nullptr);
        }

        if (server.emojis.size() < 50)
        {
            load_emojis(box);
        }
    }

    void EmojiWidget::size_allocate_vfunc(int width, int height, int baseline)
    {
        Gdk::Rectangle rect = Gdk::Rectangle();
        rect.set_x(0);
        rect.set_y(0);
        rect.set_width(width);
        rect.set_height(height);

        if (m_child)
        {
            m_child->size_allocate(rect, baseline);
        }
    }

    void EmojiWidget::measure_vfunc(Gtk::Orientation orientation, int for_size, int& minimum, int& natural, int& minimum_baseline, int& natural_baseline) const
    {
        if (m_child)
        {
            int childMinimum, childNatural, childMinBaseLine, childNatBaseline;
            m_child->measure(orientation, for_size, childMinimum, childNatural, childMinBaseLine, childNatBaseline);
            natural = minimum = std::max(minimum, childMinimum);
        }
    }

    void EmojiWidget::snapshot_vfunc(const Glib::RefPtr<Gtk::Snapshot>& snapshot)
    {
        if (m_child)
        {
            snapshot_child(*m_child, snapshot);
        }
    }

    void EmojiWidget::load_emojis(Gtk::Box* box)
    {
        auto emoji_list = s_emojis[m_load_index];

        if (m_sub_load_index == 0)
        {
            auto label = new Gtk::Label(emoji_list.name);
            label->set_xalign(0);
            label->set_margin_top(10);
            label->set_margin_start(10);
            label->set_margin_bottom(5);
            label->add_css_class("emoji-header");
            box->append(*label);

            m_loading_flow_box = new Gtk::FlowBox();
            m_loading_flow_box->set_max_children_per_line(10);
            m_loading_flow_box->set_valign(Gtk::Align::START);
            //m_loading_flow_box->set_homogeneous(true);
            box->append(*m_loading_flow_box);
        }

        int end_it = std::min(m_sub_load_index + 20, (int)emoji_list.emojis.size());
        for (; m_sub_load_index < end_it; ++m_sub_load_index)
        {
            const std::string& emoji = emoji_list.emojis[m_sub_load_index];
            auto label = new Gtk::Label();
            label->set_markup(util::Helpers::str_format("<span size='20000'>%s</span>", emoji.c_str()));
            auto button = new Gtk::Button();
            button->set_child(*label);
            button->remove_css_class("text-button");
            button->add_css_class("image-button");
            button->add_css_class("flat");
            button->signal_clicked().connect([this, emoji]()
                                             {
                                                 m_signal_emoji_selected.emit(emoji);
                                             });

            m_loading_flow_box->append(*button);
        }

        if (m_sub_load_index >= (int)emoji_list.emojis.size() - 1)
        {
            m_loading_flow_box = nullptr;
            m_sub_load_index = 0;
            ++m_load_index;
        }
    }
}