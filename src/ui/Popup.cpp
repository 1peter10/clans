#include "Popup.h"
#include "Application.h"

namespace ui
{
    void popup_on_application_flap_folded(AdwFlap*, GParamSpec* ,gpointer user_data)
    {
        ((Popup*)user_data)->on_flap_fold_state_changed();
    }

    void Popup::create_box()
    {
        Application::Get()->get_popup_backer()->set_visible(true);

        m_box->add_css_class("popup");
        m_box->add_css_class("card");

        auto revealer = Application::Get()->get_popup_revealer();
        revealer->set_child(*m_box);
    }

    void Popup::create_popover(Gtk::Widget* parent)
    {
        m_popover = new Gtk::Popover();
        m_popover->set_size_request(360, -1);
        m_popover->set_parent(*parent);
        m_popover->set_child(*m_box);
        m_popover->set_position(Gtk::PositionType::TOP);
        if (m_override_x >= 0 && m_override_y >= 0)
        {
            m_popover->set_pointing_to(Gdk::Rectangle(m_override_x - 1, m_override_y - 1, 2, 2));
        }
        m_popover->signal_closed().connect([this]()
        {
            Application::Get()->set_showing_popup(nullptr);
            m_popover->unparent();
            delete m_popover;
            m_popover = nullptr;
            g_signal_handler_disconnect(Application::Get()->get_flap(), m_flap_folded_signal_handler);
            delete this;
        });
    }

    void Popup::on_flap_fold_state_changed()
    {
        close(false);
    }

    Popup::Popup(Gtk::Widget* parent, int override_x, int override_y)
    {
        m_parent = parent;
        m_override_x = override_x;
        m_override_y = override_y;

        auto flap = Application::Get()->get_flap();
        bool flap_folded = adw_flap_get_folded(flap);
        bool flap_revealed = adw_flap_get_reveal_flap(flap);

        m_box = new Gtk::Box(Gtk::Orientation::VERTICAL);

        if (!flap_folded || flap_revealed)
        {
            create_popover(m_parent);
        }
        else
        {
           create_box();
        }

        m_flap_folded_signal_handler = g_signal_connect(flap, "notify::folded", G_CALLBACK(popup_on_application_flap_folded), (gpointer)this);
    }

    void Popup::open()
    {
        if (m_popover)
        {
            m_popover->popup();
        }
        else
        {
            auto revealer = Application::Get()->get_popup_revealer();
            revealer->set_transition_duration(250);
            revealer->set_reveal_child(true);
        }

        Application::Get()->set_showing_popup(this);
    }

    void Popup::close(bool snap)
    {
        if (m_popover)
        {
            m_popover->popdown();
        }
        else
        {
            auto revealer = Application::Get()->get_popup_revealer();
            revealer->set_transition_duration(snap ? 0 : 250);
            revealer->set_reveal_child(false);
            Glib::signal_timeout().connect([this, revealer]()
            {
                Application::Get()->get_popup_backer()->set_visible(Application::Get()->get_showing_popup());
                m_box->unparent();
                g_signal_handler_disconnect(Application::Get()->get_flap(), m_flap_folded_signal_handler);
                delete this;
                return false;
            }, revealer->get_transition_duration());
        }

        signal_closed().emit();
        Application::Get()->set_showing_popup(nullptr);
    }
}
