#pragma once

namespace ui
{
    class AttachmentArea;
    class MessageBar : public Gtk::Widget
    {
    public:
        MessageBar();
        ~MessageBar() override;

        sigc::signal<void(const std::string&, std::vector<std::string>, SleepyDiscord::Message)> signal_send() { return m_signal_send_message; }
        std::string get_text() const;
        void clear_text();
        void append_text(const std::string& text);
        void set_replying_to(const SleepyDiscord::Message& message);

        void clear_reply_status();
    private:

        void size_allocate_vfunc(int width, int height, int baseline) override;
        void measure_vfunc(Gtk::Orientation orientation, int for_size, int& minimum, int& natural, int& minimum_baseline, int& natural_baseline) const override;
        void snapshot_vfunc(const Glib::RefPtr<Gtk::Snapshot>& snapshot) override;

        Gtk::Box* m_child = nullptr;
        Gtk::TextView* m_text_view = nullptr;
        ui::AttachmentArea* m_attachment_area = nullptr;

        Gtk::Box* m_replying_to_box = nullptr;
        Gtk::Label* m_reply_name_label = nullptr;
        SleepyDiscord::Message m_replying_to_message;

        sigc::signal<void(const std::string&, std::vector<std::string>, SleepyDiscord::Message)> m_signal_send_message;
    };
}
