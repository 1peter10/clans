#pragma once

#include "Page.h"
#include "discord/objects/MessageWidgetData.h"
#include "ReverseViewport.h"

namespace util
{
    class SimpleThread;
}

namespace ui
{
    class MessageWidget;
    class MessageView : public Page
    {
    public:
        MessageView();
        ~MessageView() override;

        void on_discord_client_ready();

        void set_channel(SleepyDiscord::Channel channel);
        SleepyDiscord::Channel get_channel() { return m_channel; }

        sigc::signal<void(double, double)>& signal_scroll_changed() { return m_signal_scroll_changed; };

        void create_header_bar() override;

        void on_add_reaction(const SleepyDiscord::Snowflake<SleepyDiscord::Message>& message, const SleepyDiscord::Emoji& emoji, bool me);
        void on_delete_reaction(const SleepyDiscord::Snowflake<SleepyDiscord::Message>& message, const SleepyDiscord::Emoji& emoji, bool me);
    private:
        void size_allocate_vfunc(int width, int height, int baseline) override;
        void measure_vfunc(Gtk::Orientation orientation, int for_size, int& minimum, int& natural, int& minimum_baseline, int& natural_baseline) const override;
        void snapshot_vfunc(const Glib::RefPtr<Gtk::Snapshot>& snapshot) override;

        void get_messages();
        std::vector<Glib::RefPtr<discord::MessageWidgetData>> prepare_message_data(const SleepyDiscord::Message& message, const SleepyDiscord::Message* previous_message);

        Gtk::Box* m_child = nullptr;
        Gtk::ScrolledWindow* m_scrolled_window = nullptr;
        ReverseViewport* m_viewport = nullptr;
        Gtk::Box* m_messages_box = nullptr;
        Gtk::Label* m_title = nullptr;
        MessageWidget* m_loading_widget = nullptr;
        std::vector<SleepyDiscord::Message> m_pending_messages;

        SleepyDiscord::Channel m_channel;
        SleepyDiscord::Snowflake<SleepyDiscord::Message> m_last_message_id;
        util::SimpleThread* m_messages_thread = nullptr;
        sigc::connection m_scroll_timeout_connection;
        sigc::connection m_add_messages_timeout_connection;

        sigc::signal<void(double, double)> m_signal_scroll_changed;

        // Signal handlers
        sigc::connection m_new_message_connection;
    };
}
