#include <Application.h>
#include "ImageViewer.h"

namespace ui
{
    void ImageViewer::size_allocate_vfunc(int width, int height, int baseline)
    {
        Gdk::Rectangle rect = Gdk::Rectangle();
        rect.set_x(0);
        rect.set_y(0);
        rect.set_width(width);
        rect.set_height(height);

        if (m_child)
        {
            m_child->size_allocate(rect, baseline);
        }
    }

    void ImageViewer::measure_vfunc(Gtk::Orientation orientation, int for_size, int& minimum, int& natural, int& minimum_baseline, int& natural_baseline) const
    {
        if (m_child)
        {
            int childMinimum, childNatural, childMinBaseLine, childNatBaseline;
            m_child->measure(orientation, for_size, childMinimum, childNatural, childMinBaseLine, childNatBaseline);
            natural = minimum = std::max(minimum, childMinimum);
        }
    }

    void ImageViewer::snapshot_vfunc(const Glib::RefPtr<Gtk::Snapshot>& snapshot)
    {
        if (m_child)
        {
            snapshot_child(*m_child, snapshot);
        }

        if (m_texture)
        {
            calculate_image_bounds();

            snapshot->push_clip(m_image_box->get_allocation());
            graphene_point_t box_min{ (float) m_image_box->get_allocation().get_x() + m_image_min.x, (float) m_image_box->get_allocation().get_y() + m_image_min.y };
            gtk_snapshot_translate(snapshot->gobj(), &box_min);
            //gtk_snapshot_scale(snapshot->gobj(), m_scale, m_scale);
            m_texture->snapshot(snapshot, m_image_max.x - m_image_min.x, m_image_max.y - m_image_min.y);
            snapshot->pop();
        }
    }

    ImageViewer::ImageViewer()
            : Page()
    {
        m_child = new Gtk::Box();
        m_child->set_orientation(Gtk::Orientation::VERTICAL);
        m_child->set_parent(*this);

        m_image_box = new Gtk::Box();
        m_image_box->set_vexpand(true);
        m_image_box->set_hexpand(true);
        m_child->append(*m_image_box);

        add_css_class("image_viewer");

        auto drag = Gtk::GestureDrag::create();
        m_image_box->add_controller(drag);
        drag->signal_drag_update().connect([this, drag](double x, double y)
                                           {
                                               m_drag_offset_x = x;
                                               m_drag_offset_y = y;

                                               queue_draw();
                                           });

        drag->signal_drag_end().connect([this, drag](double, double)
                                        {
                                            m_offset_x += m_drag_offset_x;
                                            m_offset_y += m_drag_offset_y;
                                            m_drag_offset_x = 0;
                                            m_drag_offset_y = 0;
                                        });

        Glib::RefPtr<Gtk::GestureZoom> zoom = Gtk::GestureZoom::create();
        m_image_box->add_controller(zoom);
        zoom->set_propagation_phase(Gtk::PropagationPhase::CAPTURE);
        zoom->signal_scale_changed().connect([this, zoom](double scale)
                                             {
                                                 double zoom_pos_x, zoom_pos_y;
                                                 zoom->get_bounding_box_center(zoom_pos_x, zoom_pos_y);
                                                 double old_dist_x = zoom_pos_x - m_offset_x - m_drag_offset_x;
                                                 double old_dist_y = zoom_pos_y - m_offset_y - m_drag_offset_y;

                                                 float old_scale = m_scale;
                                                 m_scale = m_pre_zoom_scale * scale;
                                                 if (m_scale < 1.f)
                                                 {
                                                     m_scale = 1.f;
                                                 }

                                                 float scale_factor = m_scale / old_scale;
                                                 double dx = old_dist_x * (scale_factor - 1.f);
                                                 double dy = old_dist_y * (scale_factor - 1.f);
                                                 m_offset_x -= dx;
                                                 m_offset_y -= dy;

                                                 adw_flap_set_swipe_to_open(Application::Get()->get_flap(), m_scale <= 1.f);
                                                 adw_leaflet_set_can_navigate_back(Application::Get()->get_leaflet(), m_scale <= 1.f);
                                                 queue_draw();
                                             });

        zoom->signal_begin().connect([this](Gdk::EventSequence*)
                                     {
                                         m_pre_zoom_scale = m_scale;
                                     });

        auto motion = Gtk::EventControllerMotion::create();
        m_image_box->add_controller(motion);
        motion->signal_motion().connect([this, motion](double x, double y)
                                        {
                                            m_mouse_x = x;
                                            m_mouse_y = y;
                                        });

        Glib::RefPtr<Gtk::EventControllerScroll> scroll = Gtk::EventControllerScroll::create();
        m_image_box->add_controller(scroll);
        scroll->set_flags(Gtk::EventControllerScroll::Flags::VERTICAL);
        scroll->set_propagation_phase(Gtk::PropagationPhase::CAPTURE);
        scroll->signal_scroll().connect([this](double, double y)
                                        {
                                            double old_dist_x = m_mouse_x - m_offset_x;
                                            double old_dist_y = m_mouse_y - m_offset_y;

                                            float old_scale = m_scale;
                                            m_scale += (float) -y * 0.1f;
                                            if (m_scale < 1.f)
                                            {
                                                m_scale = 1.f;
                                            }
                                            float scale_factor = m_scale / old_scale;
                                            double dx = old_dist_x * (scale_factor - 1.f);
                                            double dy = old_dist_y * (scale_factor - 1.f);
                                            m_offset_x -= dx;
                                            m_offset_y -= dy;

                                            adw_flap_set_swipe_to_open(Application::Get()->get_flap(), m_scale <= 1.f);
                                            adw_leaflet_set_can_navigate_back(Application::Get()->get_leaflet(), m_scale <= 1.f);
                                            queue_draw();
                                            return true;
                                        }, true);
    }

    void ImageViewer::set_texture(const Glib::RefPtr<Gdk::Texture>& texture)
    {
        m_texture = texture;
        m_aspect = (float) m_texture->get_width() / (float) m_texture->get_height();
        queue_draw();
    }

    void ImageViewer::calculate_image_bounds()
    {
        float frame_width = (float) m_image_box->get_allocated_width();
        float frame_height = (float) m_image_box->get_allocated_height();

        float image_height = 0.f, height_diff = 0.f, image_width = 0.f, width_diff = 0.f;

        float frame_aspect = frame_width / frame_height;
        if (frame_aspect < m_aspect)
        {
            image_height = frame_width * m_scale / m_aspect;
            height_diff = frame_height - image_height;
            if (m_scale <= 1.f && height_diff < 0)
            {
                image_height += height_diff;
                height_diff = 0.f;
            }
            image_width = image_height * m_aspect;
            width_diff = frame_width - image_width;
        } else
        {
            image_width = frame_height * m_scale * m_aspect;
            width_diff = frame_width - image_width;
            if (m_scale <= 1.f && width_diff < 0)
            {
                image_width += width_diff;
                width_diff = 0.f;
            }
            image_height = image_width / m_aspect;
            height_diff = frame_height - image_height;
        }

        m_image_min = { m_drag_offset_x + m_offset_x, m_drag_offset_y + m_offset_y };

        if (width_diff > 0)
        {
            m_image_min.x = width_diff / 2;
        }
        if (height_diff > 0)
        {
            m_image_min.y = height_diff / 2;
        }

        m_image_max = { m_image_min.x + image_width, m_image_min.y + image_height };

        float x_threshold = width_diff > 0.f ? width_diff / 2.f : 0;

        if (m_image_min.x > x_threshold)
        {
            m_drag_offset_x = 0;
            m_offset_x = x_threshold;
            m_image_min.x = m_offset_x;
            m_image_max.x = m_image_min.x + image_width;
        }

        float y_threshold = height_diff > 0.f ? height_diff / 2.f : 0;
        if (m_image_min.y > y_threshold)
        {
            m_drag_offset_y = 0;
            m_offset_y = y_threshold;
            m_image_min.y = m_offset_y;
            m_image_max.y = m_image_min.y + image_height;
        }

        float x_max_threshold = width_diff > 0.f ? frame_width - width_diff / 2.f : frame_width;

        if (m_image_max.x < x_max_threshold)
        {
            m_drag_offset_x = 0;
            m_offset_x = x_max_threshold - image_width;
            m_image_min.x = m_offset_x;
            m_image_max.x = x_max_threshold;
        }

        float y_max_threshold = height_diff > 0.f ? frame_height - height_diff / 2.f : frame_height;
        if (m_image_max.y < y_max_threshold)
        {
            m_drag_offset_y = 0;
            m_offset_y = y_max_threshold - image_height;
            m_image_min.y = m_offset_y;
            m_image_max.y = y_max_threshold;
        }
    }

    void ImageViewer::create_header_bar()
    {
        m_header_bar = (GtkHeaderBar*) adw_header_bar_new();
        adw_header_bar_set_show_start_title_buttons((AdwHeaderBar*) m_header_bar, false);
        adw_header_bar_set_title_widget((AdwHeaderBar*) m_header_bar, (GtkWidget*) gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0));

        add_flap_button(true);

        auto button = new Gtk::Button();
        button->set_icon_name("go-previous-symbolic");
        button->signal_clicked().connect([]()
                                         {
                                             Application::Get()->navigate_backwards();
                                         });
        adw_header_bar_pack_start((AdwHeaderBar*) m_header_bar, (GtkWidget*) button->gobj());

        gtk_box_prepend(m_child->gobj(), (GtkWidget*) m_header_bar);
    }
}