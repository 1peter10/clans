#include "Popup.h"

namespace ui
{
    class EmojiPopup : public Popup
    {
    public:
        EmojiPopup(Gtk::Widget* parent, std::function<void(const std::string&)> on_selected, std::function<void(const SleepyDiscord::Emoji&)> on_server_emoji_selected, int override_x = -1, int override_y = -1);
    };
}
