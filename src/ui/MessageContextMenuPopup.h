#include "Popup.h"

namespace ui
{
    class MessageWidget;
    class MessageContextMenuPopup : public Popup
    {
    public:
        MessageContextMenuPopup(Gtk::Widget* parent, MessageWidget* message_widget, int override_x = -1, int override_y = -1);

    private:
        void create_action(const std::string& name, const std::string& icon, std::function<void()> action);

        Gtk::ListBox* m_list_box = nullptr;
    };
}