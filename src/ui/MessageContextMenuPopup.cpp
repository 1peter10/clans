#include <Application.h>
#include "MessageContextMenuPopup.h"
#include "EmojiPopup.h"
#include "MessageWidget.h"
#include "discord/DiscordClient.h"
#include "MessageBar.h"
namespace ui
{
    struct ContextMenuAction
    {
        std::function<void()> action;
    };

    void message_context_menu_row_activated(AdwActionRow*, gpointer action)
    {
        ((ContextMenuAction*)action)->action();
    }

    MessageContextMenuPopup::MessageContextMenuPopup(Gtk::Widget* parent, MessageWidget* message_widget, int override_x, int override_y)
        : Popup(parent, override_x, override_y)
    {
        m_list_box = new Gtk::ListBox();
        if (!m_popover)
        {
            m_list_box->set_margin_top(10);
        }
        m_list_box->set_selection_mode(Gtk::SelectionMode::NONE);
        if (m_popover)
        {
            m_box->add_css_class("popover-item-background");
            m_list_box->add_css_class("popover-item-background");
        }
        m_box->append(*m_list_box);

        create_action("Add Reaction", "face-smile-symbolic", [this, message_widget]()
        {
            Application::Get()->increment_popup_background_persist_count();
            auto parent = m_parent;
            close(true);
            new EmojiPopup(parent, [message_widget](const std::string& emoji)
           {
               Application::Get()->get_discord_client()->addReaction(message_widget->get_message().channelID, message_widget->get_message().ID, emoji);
           }, [message_widget](const SleepyDiscord::Emoji& emoji)
           {
               std::string url = util::Helpers::str_format("%s:%s", emoji.name.c_str(), emoji.ID.string().c_str());
               Application::Get()->get_discord_client()->addReaction(message_widget->get_message().channelID, message_widget->get_message().ID, url);
           }, m_override_x, m_override_y);
            Application::Get()->decrement_popup_background_persist_count();
        });

        create_action("Reply", "mail-reply-sender-symbolic", [this, message_widget]()
        {
            Application::Get()->get_message_bar()->set_replying_to(message_widget->get_message());
            close(false);
        });
        create_action("Pin Message (TODO)", "view-pin-symbolic", [this](){});
        create_action("Create Thread (TODO)", "format-indent-more-symbolic", [this](){});
        create_action("Mark Unread (TODO)", "mail-unread-symbolic", [this](){});
        create_action("Copy Message Link (TODO)", "emblem-shared-symbolic", [this](){});
        create_action("Delete Message (TODO)", "user-trash-symbolic", [this](){});

        open();
    }

    void MessageContextMenuPopup::create_action(const std::string& name, const std::string& icon, std::function<void()> cb)
    {
        GtkWidget* row = adw_action_row_new();
        adw_preferences_row_set_title((AdwPreferencesRow*)row, name.c_str());
        g_object_set(row, "activatable", true, (const char*)nullptr);

        ContextMenuAction* action = new ContextMenuAction();
        action->action = cb;
        g_signal_connect(row, "activated", G_CALLBACK(message_context_menu_row_activated), action);

        gtk_list_box_append(m_list_box->gobj(), (GtkWidget*)row);

        adw_action_row_set_icon_name((AdwActionRow*)row, icon.c_str());

        gtk_widget_add_css_class(gtk_widget_get_first_child(row), "context-menu-item");
        if (m_popover)
        {
            gtk_widget_add_css_class(row, "popover-item-background-activatable");
        }
        else
        {
            gtk_widget_add_css_class(row, "background");
        }
    }
}