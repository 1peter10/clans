#include <util/Helpers.h>
#include <util/ImageCache.h>
#include "UserWidget.h"
#include "util/CancellationToken.h"

namespace ui
{
    UserWidget::UserWidget(const SleepyDiscord::User& user)
    {
        m_user = user;

        m_child = new Gtk::Box(Gtk::Orientation::HORIZONTAL, 20);
        m_child->set_parent(*this);

        GtkWidget* avatar = adw_avatar_new(32, user.username.c_str(), true);
        gtk_widget_set_valign(avatar, GTK_ALIGN_START);
        gtk_widget_set_margin_start(avatar, 5);
        gtk_widget_set_margin_end(avatar, 10);
        gtk_box_prepend(m_child->gobj(), avatar);
        if (!user.avatar.empty())
        {
            std::string avatar_url = util::Helpers::str_format("https://cdn.discordapp.com/avatars/%s/%s.png?size=64", user.ID.string().c_str(), user.avatar.c_str());
            m_avatar_cancellation_token = new util::CancellationToken();
            util::ImageCache::get()->get_image(avatar_url, [this, avatar](const std::string& file_path)
            {
                try
                {
                    auto texture = Gdk::Texture::create_from_file(Gio::File::create_for_path(file_path));
                    adw_avatar_set_custom_image((AdwAvatar*) avatar, (GdkPaintable*) texture->gobj());
                }
                catch (std::exception& e)
                {
                    std::cout << e.what() << std::endl;
                }

                m_avatar_cancellation_token = nullptr;
            }, m_avatar_cancellation_token);
        }

        auto label = new Gtk::Label(user.username);
        label->add_css_class("bold");
        m_child->append(*label);
    }

    UserWidget::~UserWidget()
    {
        if (m_avatar_cancellation_token)
        {
            m_avatar_cancellation_token->cancel();
            m_avatar_cancellation_token = nullptr;
        }
    }

    void UserWidget::size_allocate_vfunc(int width, int height, int baseline)
    {
        Gdk::Rectangle rect = Gdk::Rectangle();
        rect.set_x(0);
        rect.set_y(0);
        rect.set_width(width);
        rect.set_height(height);

        if (m_child)
        {
            m_child->size_allocate(rect, baseline);
        }
    }

    void UserWidget::measure_vfunc(Gtk::Orientation orientation, int for_size, int& minimum, int& natural, int& minimum_baseline, int& natural_baseline) const
    {
        if (m_child)
        {
            m_child->measure(orientation, for_size, minimum, natural, minimum_baseline, natural_baseline);
        }
    }

    void UserWidget::snapshot_vfunc(const Glib::RefPtr<Gtk::Snapshot>& snapshot)
    {
        if (m_child)
        {
            snapshot_child(*m_child, snapshot);
        }
    }
}