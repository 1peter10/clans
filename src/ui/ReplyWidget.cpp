#include "ReplyWidget.h"
#include "util/Helpers.h"
#include "util/ImageDownloader.h"
#include "util/HtmlMetaParser.h"
#include <regex>
#include <filesystem>
#include "util/SimpleThread.h"
#include "discord/DiscordClient.h"
#include "util/ImageCache.h"
#include "util/CancellationToken.h"
#include "EmojiPopup.h"
#include "MessageContextMenuPopup.h"

namespace ui
{
    ReplyWidget::ReplyWidget(const SleepyDiscord::ReferencedMessage& message)
            : Gtk::Widget()
              , m_message(message)
    {
        add_css_class("message-widget");

        set_margin_start(5);
        set_margin_end(5);

        m_child = new Gtk::Box(Gtk::Orientation::HORIZONTAL, 0);
        m_child->set_parent(*this);

        auto image = new Gtk::Image();
        image->set_margin_start(12);
        image->set_margin_end(10);
        image->set_from_icon_name("mail-forward-symbolic");
        m_child->append(*image);

        set_margin_top(10);

        GtkWidget* avatar = adw_avatar_new(16, m_message.author.username.c_str(), true);
        gtk_widget_set_margin_end(avatar, 5);
        gtk_box_append(m_child->gobj(), avatar);

        auto name_label = new Gtk::Label();
        name_label->set_text(m_message.author.username);
        name_label->set_xalign(0);
        name_label->set_margin_end(5);
        name_label->add_css_class("message_reply_title");
        m_child->append(*name_label);

        if (!m_message.author.avatar.empty())
        {
            std::string avatar_url = util::Helpers::str_format("https://cdn.discordapp.com/avatars/%s/%s.png?size=64", m_message.author.ID.string().c_str(), m_message.author.avatar.c_str());
            m_avatar_cancellation_token = new util::CancellationToken();
            util::ImageCache::get()->get_image(avatar_url, [this, avatar](const std::string& file_path)
            {
                try
                {
                    auto texture = Gdk::Texture::create_from_file(Gio::File::create_for_path(file_path));
                    adw_avatar_set_custom_image((AdwAvatar*) avatar, (GdkPaintable*) texture->gobj());
                    gtk_widget_set_opacity(avatar, 0.6);
                }
                catch (std::exception& e)
                {
                    std::cout << e.what() << std::endl;
                }

                m_avatar_cancellation_token = nullptr;
            }, m_avatar_cancellation_token);
        }

        if (!m_message.content.empty())
        {
            const char* escaped_text = g_markup_escape_text(m_message.content.c_str(), (gssize) m_message.content.length());
            std::vector<std::string> links = util::Helpers::extract_urls_from_str(escaped_text);
            std::string message_content = escaped_text;

            for (const auto& link: links)
            {
                std::string markup_link = util::Helpers::str_format("<a href=\"%s\">%s</a>", link.c_str(), link.c_str());
                message_content.replace(message_content.find(link), link.length(), markup_link);
            }

            auto label = new Gtk::Label();
            label->set_lines(1);
            label->set_overflow(Gtk::Overflow::HIDDEN);
            label->set_opacity(0.6);
            label->set_ellipsize(Pango::EllipsizeMode::END);
            for (const auto& mention: m_message.mentions)
            {
                std::string search_string = util::Helpers::str_format("&lt;@!%s&gt;", mention.ID.string().c_str());
                auto it = message_content.find(search_string);

                if (it == std::string::npos)
                {
                    std::string search_string = util::Helpers::str_format("&lt;@%s&gt;", mention.ID.string().c_str());
                    it = message_content.find(search_string);
                    if (it == std::string::npos)
                    {
                        continue;
                    }
                }

                std::string colour = adw_style_manager_get_dark(adw_style_manager_get_default()) ? "darkblue" : "lightblue";
                std::string replacement = util::Helpers::str_format(R"( <span background="%s">@%s</span> )", colour.c_str(), mention.username.c_str());
                message_content = message_content.replace(it, search_string.size(), replacement);
            }

            label->set_use_markup(true);
            label->set_markup(message_content);
            label->set_wrap_mode(Pango::WrapMode::WORD_CHAR);
            label->set_wrap(true);
            label->set_margin_top(5);
            label->set_margin_bottom(5);
            label->set_margin_end(5);
            label->set_xalign(0.f);
            label->add_css_class("message_text");

            m_child->append(*label);
        }
        else
        {
            auto label = new Gtk::Label();
            label->set_opacity(0.6);
            label->set_use_markup(true);
            label->set_markup("<i>attachment</i>");
            label->set_margin_top(5);
            label->set_margin_bottom(5);
            label->set_margin_end(5);
            label->set_xalign(0.f);
            label->add_css_class("message_text");

            m_child->append(*label);
        }
    }

    ReplyWidget::~ReplyWidget()
    {
        m_child->unparent();
        delete m_child;

        if (m_avatar_cancellation_token)
        {
            m_avatar_cancellation_token->cancel();
            m_avatar_cancellation_token = nullptr;
        }
    }

    void ReplyWidget::size_allocate_vfunc(int width, int height, int baseline)
    {
        Gdk::Rectangle rect = Gdk::Rectangle();
        rect.set_x(0);
        rect.set_y(0);
        rect.set_width(width);
        rect.set_height(height);

        if (m_child)
        {
            m_child->size_allocate(rect, baseline);
        }
    }

    void ReplyWidget::measure_vfunc(Gtk::Orientation orientation, int for_size, int& minimum, int& natural, int& minimum_baseline, int& natural_baseline) const
    {
        if (m_child)
        {
            m_child->measure(orientation, for_size, minimum, natural, minimum_baseline, natural_baseline);
        }
    }

    void ReplyWidget::snapshot_vfunc(const Glib::RefPtr<Gtk::Snapshot>& snapshot)
    {
        if (m_child)
        {
            snapshot_child(*m_child, snapshot);
        }
    }

    Gtk::SizeRequestMode ReplyWidget::get_request_mode_vfunc() const
    {
        return Gtk::SizeRequestMode::HEIGHT_FOR_WIDTH;
    }
}