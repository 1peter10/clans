#include "GifWidget.h"
#include "Application.h"
#include "util/Helpers.h"
#include "MessageView.h"
#include "MessageWidget.h"

namespace ui
{
    GifWidget::GifWidget(ui::MessageWidget* parent)
        : Gtk::Widget()
        , m_parent_message(parent)
    {
        m_child = new Gtk::Picture();
        m_child->set_parent(*this);

        m_scroll_connection = Application::Get()->get_message_view()->signal_scroll_changed().connect([this](double viewport_top, double viewport_bottom)
        {
            if (!m_animation || !m_iter)
            {
                return;
            }

            if (!m_parent_message || !m_parent_message->get_parent())
            {
                return;
            }

            int buffer = m_parent_message->get_allocated_height() / 2;
            int top = m_parent_message->get_allocation().get_y();
            int bottom = top + m_parent_message->get_allocated_height();
            if (viewport_top > top + buffer || viewport_bottom < bottom - buffer)
            {
                pause();
            }
            else if (m_paused)
            {
                play(false);
            }
        });
    }

    GifWidget::~GifWidget()
    {
        if (m_timeout_connection.connected())
        {
            m_timeout_connection.disconnect();
        }

        m_scroll_connection.disconnect();

        m_child->unparent();
    }

    void GifWidget::set_gif(const std::string& file_path)
    {
        try
        {
            m_animation = Gdk::PixbufAnimation::create_from_file(file_path);
            set_size_request(-1, std::min(m_animation->get_height(), 240));

            m_iter = m_animation->get_iter();
            play(m_paused);
        }
        catch (std::exception& e)
        {
            std::cout << e.what() << std::endl;
        }
    }

    void GifWidget::size_allocate_vfunc(int width, int height, int baseline)
    {
        Gdk::Rectangle rect = Gdk::Rectangle();
        rect.set_x(0);
        rect.set_y(0);
        rect.set_width(width);
        rect.set_height(height);

        if (m_child)
        {
            m_child->size_allocate(rect, baseline);
        }
    }

    void GifWidget::measure_vfunc(Gtk::Orientation orientation, int for_size, int& minimum, int& natural, int& minimum_baseline, int& natural_baseline) const
    {
        if (m_child)
        {
            m_child->measure(orientation, for_size, minimum, natural, minimum_baseline, natural_baseline);
        }
    }

    void GifWidget::snapshot_vfunc(const Glib::RefPtr<Gtk::Snapshot>& snapshot)
    {
        if (m_child)
        {
            snapshot_child(*m_child, snapshot);
        }
    }

    void GifWidget::play(bool once)
    {
        m_paused = once;

        m_timeout_connection = Glib::signal_timeout().connect([this]()
        {
            get_iter()->advance();
            get_picture()->set_pixbuf(get_iter()->get_pixbuf());
            queue_draw();
            return !m_paused;
        }, m_iter->get_delay_time());
    }
}