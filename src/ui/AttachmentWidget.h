#pragma once

namespace ui
{
    class AttachmentArea;
    class AttachmentWidget : public Gtk::Widget
    {
    public:
        AttachmentWidget(AttachmentArea* parent);
        AttachmentWidget(AttachmentArea* parent, const std::string& file_path);

        const std::string get_filepath() const { return m_filepath; }

    private:
        void size_allocate_vfunc(int width, int height, int baseline) override;
        void measure_vfunc(Gtk::Orientation orientation, int for_size, int& minimum, int& natural, int& minimum_baseline, int& natural_baseline) const override;
        void snapshot_vfunc(const Glib::RefPtr<Gtk::Snapshot>& snapshot) override;

        Gtk::Widget* m_child = nullptr;
        Gtk::Picture* m_picture = nullptr;

        std::string m_filepath;
    };
}
