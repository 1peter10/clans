#pragma once

namespace util
{
    class CancellationToken;
}
namespace ui
{
    class UserWidget : public Gtk::Widget
    {
    public:
        UserWidget(const SleepyDiscord::User& user);
        ~UserWidget();

        const SleepyDiscord::User& get_user() const { return m_user; }
    private:
        void size_allocate_vfunc(int width, int height, int baseline) override;
        void measure_vfunc(Gtk::Orientation orientation, int for_size, int& minimum, int& natural, int& minimum_baseline, int& natural_baseline) const override;
        void snapshot_vfunc(const Glib::RefPtr<Gtk::Snapshot>& snapshot) override;

        SleepyDiscord::User m_user;
        Gtk::Box* m_child;
        util::CancellationToken* m_avatar_cancellation_token = nullptr;
    };
}