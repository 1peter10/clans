#pragma once

#include "Popup.h"

namespace util
{
    class SimpleThread;
}
namespace ui
{
    class ReactionsPopup : public Popup
    {
    public:
        ReactionsPopup(const SleepyDiscord::Message& message, const SleepyDiscord::Reaction& reaction, Gtk::Widget* parent, int override_x = -1, int override_y = -1);
        ~ReactionsPopup();
    private:
        void populate_users_for_page(int index, Gtk::Box* page);

        struct PageData
        {
            bool loaded;
            Gtk::Box* page;
            SleepyDiscord::Message message;
            SleepyDiscord::Reaction reaction;
            util::SimpleThread* thread;
        };
        std::vector<PageData> m_page_data;
    };
}