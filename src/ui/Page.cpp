#include "Page.h"
#include "Application.h"

namespace ui
{
    Page::~Page()
    {
        auto flap = Application::Get()->get_flap();

        for (guint signal_handler : m_signal_handlers)
        {
            g_signal_handler_disconnect(flap, signal_handler);
        }
    }

    void on_application_flap_folded(AdwFlap*, GParamSpec* ,gpointer user_data)
    {
        auto page = (Page*)user_data;
        page->update_flap_button_visibility();
    }

    void Page::update_flap_button_visibility()
    {
        auto flap = Application::Get()->get_flap();
        bool flap_folded = adw_flap_get_folded(flap);
        bool flap_revealed = adw_flap_get_reveal_flap(flap);

        if (m_only_show_flap_button_when_not_folded)
        {
            m_flap_button->set_visible(!flap_folded && !flap_revealed);
        }
        else
        {
            m_flap_button->set_visible(flap_folded || !flap_revealed);
        }
    }

    void Page::add_flap_button(bool only_when_not_folded)
    {
        auto flap = Application::Get()->get_flap();

        m_flap_button = new Gtk::Button();
        m_flap_button->set_icon_name("view-sidebar-start-symbolic");
        m_flap_button->signal_clicked().connect([flap]()
        {
            adw_flap_set_reveal_flap(flap, !adw_flap_get_reveal_flap(flap));
        });
        m_only_show_flap_button_when_not_folded = only_when_not_folded;
        update_flap_button_visibility();
        adw_header_bar_pack_start((AdwHeaderBar*)m_header_bar, (GtkWidget*)m_flap_button->gobj());

        guint signal_handler = g_signal_connect(flap, "notify::folded", G_CALLBACK(on_application_flap_folded), (gpointer)this);
        m_signal_handlers.push_back(signal_handler);

        signal_handler = g_signal_connect(flap, "notify::reveal-flap", G_CALLBACK(on_application_flap_folded), (gpointer)this);
        m_signal_handlers.push_back(signal_handler);
    }
}
