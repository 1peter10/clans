#include "MessageView.h"
#include "MessageWidget.h"
#include "../Application.h"
#include "../discord/DiscordClient.h"
#include "../util/SimpleThread.h"
#include "GifWidget.h"
#include "../util/HtmlMetaParser.h"
#include "MessageBar.h"
#include "EmptyMessageView.h"

namespace ui
{
    MessageView::MessageView()
            : Page()
    {
        m_child = new Gtk::Box();
        m_child->set_orientation(Gtk::Orientation::VERTICAL);

        m_scrolled_window = new Gtk::ScrolledWindow();
        m_scrolled_window->set_vexpand(true);
        m_scrolled_window->set_placement(Gtk::CornerType::BOTTOM_LEFT);
        m_child->append(*m_scrolled_window);

        m_viewport = new ReverseViewport();
        m_viewport->set_valign(Gtk::Align::END);
        m_scrolled_window->set_child(*m_viewport);

        m_messages_box = new Gtk::Box(Gtk::Orientation::VERTICAL);
        m_viewport->set_child(m_messages_box);

        m_viewport->signal_user_scrolled().connect([this](double value)
        {
            if (m_scroll_timeout_connection.connected())
            {
                m_scroll_timeout_connection.disconnect();
            }
            if (m_add_messages_timeout_connection.connected())
            {
                m_add_messages_timeout_connection.disconnect();
            }
            if (m_messages_box->get_allocation().get_y() > (-m_scrolled_window->get_height() * 2))
            {
                if (m_loading_widget)
                {
                    m_loading_widget->set_child_visible(true);
                }

                m_scroll_timeout_connection = Glib::signal_timeout().connect([this]()
                {
                    get_messages();
                    return false;
                }, 1000);
            }

            m_signal_scroll_changed.emit(value, value + m_viewport->get_height());
        });

        if (Application::Get()->get_discord_client() && Application::Get()->get_discord_client()->is_ready())
        {
            on_discord_client_ready();
        }

        m_child->set_parent(*this);
    }

    MessageView::~MessageView()
    {
        m_new_message_connection.disconnect();
    }

    void MessageView::size_allocate_vfunc(int width, int height, int baseline)
    {
        Gdk::Rectangle rect = Gdk::Rectangle();
        rect.set_x(0);
        rect.set_y(0);
        rect.set_width(width);
        rect.set_height(height);

        if (m_child)
        {
            m_child->size_allocate(rect, baseline);
        }
    }

    void MessageView::measure_vfunc(Gtk::Orientation orientation, int for_size, int& minimum, int& natural, int& minimum_baseline, int& natural_baseline) const
    {
        if (m_child)
        {
            int childMinimum, childNatural, childMinBaseLine, childNatBaseline;
            m_child->measure(orientation, for_size, childMinimum, childNatural, childMinBaseLine, childNatBaseline);
            natural = minimum = std::max(minimum, childMinimum);
        }
    }

    void MessageView::snapshot_vfunc(const Glib::RefPtr<Gtk::Snapshot>& snapshot)
    {
        if (m_child)
        {
            snapshot_child(*m_child, snapshot);
        }
    }

    std::vector<Glib::RefPtr<discord::MessageWidgetData>> MessageView::prepare_message_data(const SleepyDiscord::Message& message, const SleepyDiscord::Message* previous_message)
    {
        std::vector<Glib::RefPtr<discord::MessageWidgetData>> ret;

        bool show_name = false;
        if (!previous_message)
        {
            show_name = true;
        }
        else
        {
            if (previous_message->author.username != message.author.username)
            {
                show_name = true;
            }
            else
            {
                std::string timestamp_old = previous_message->timestamp;
                util::DateTime date_time_old = util::Helpers::discord_time_str_to_datetime(timestamp_old);

                std::string timestamp_new = message.timestamp;
                util::DateTime date_time_new = util::Helpers::discord_time_str_to_datetime(timestamp_new);

                if (date_time_new.get_epoch_seconds() - date_time_old.get_epoch_seconds() > 300)
                {
                    show_name = true;
                }
            }
        }

        if (!message.content.empty())
        {
            auto message_data = discord::MessageWidgetData::create(message);
            message_data->set_show_name(show_name);
            ret.push_back(message_data);

            std::vector<std::string> links = util::Helpers::extract_urls_from_str(message.content);
            if (links.size() == 1 && links[0] == message.content)
            {
                new util::SimpleThread([links](util::ThreadWorker*)
                {
                    auto meta = util::HtmlMetaParser::get_metadata(links[0]);auto ret = new std::map<std::string, std::string>(meta);return (void*)ret;
                },
                [this, message_data](void* user_data, bool, util::SimpleThread*)
                {
                    auto meta = *((std::map<std::string, std::string>*)user_data);

                    if (meta.count("og:image") > 0 && meta["og:image:type"] == "image/gif")
                    {
                        message_data->property_image_url().set_value(meta["og:image"]);
                    }
                });
            }
        }

        for (int i = (int)message.attachments.size() - 1; i > -1; --i)
        {
            auto message_data = discord::MessageWidgetData::create(message);
            message_data->set_show_name(show_name);
            message_data->set_display_attachment(message.attachments[i]);
            message_data->set_hide_text(true);
            ret.push_back(message_data);
        }

        return ret;
    }

    void MessageView::set_channel(SleepyDiscord::Channel channel)
    {
        m_pending_messages.clear();
        if (m_scroll_timeout_connection.connected())
        {
            m_scroll_timeout_connection.disconnect();
        }
        if (m_add_messages_timeout_connection.connected())
        {
            m_add_messages_timeout_connection.disconnect();
        }

        set_visible(true);
        auto message_bar = Application::Get()->get_message_bar();
        message_bar->set_sensitive(false);
        message_bar->clear_reply_status();
        message_bar->clear_text();

        m_last_message_id = SleepyDiscord::Snowflake<SleepyDiscord::Message>();

        m_channel = channel;

        auto child_list = std::vector<Gtk::Widget*>();
        for (auto child = m_messages_box->get_first_child(); child != nullptr; child = child->get_next_sibling())
        {
            child_list.push_back(child);
        }
        for (auto child : child_list)
        {
            m_messages_box->remove(*child);
        }
        m_viewport->reset_offset();
        if (m_scroll_timeout_connection.connected())
        {
            m_scroll_timeout_connection.disconnect();
        }

        auto loading_obj = discord::MessageWidgetData::create();
        m_loading_widget = new MessageWidget();
        m_loading_widget->init(loading_obj);
        m_messages_box->append(*m_loading_widget);

        m_title->set_text(channel.name);

        get_messages();
    }

    void MessageView::get_messages()
    {
        if (m_messages_thread)
        {
            return;
        }

        m_messages_thread = new util::SimpleThread([this](util::ThreadWorker*)
        {
            try
            {
                if (!m_pending_messages.empty())
                {
                    return nullptr;
                }

                discord::DiscordClient* client = Application::Get()->get_discord_client();

                std::vector<SleepyDiscord::Message> messages;
                if (m_last_message_id.empty())
                {
                    messages = client->getMessages(m_channel.ID, SleepyDiscord::WebsocketppDiscordClient::GetMessagesKey::limit, m_channel.lastMessageID, 30).vector();
                }
                else
                {
                    messages = client->getMessages(m_channel.ID, SleepyDiscord::WebsocketppDiscordClient::GetMessagesKey::before, m_last_message_id, 30).vector();
                }

                if (messages.empty())
                {
                    return nullptr;
                }
                m_last_message_id = messages.back().ID;
                if (m_channel.lastMessageID != Application::Get()->get_discord_client()->get_cache().get_last_seen_message_for_channel(m_channel.ID))
                {
                    Application::Get()->get_discord_client()->ackMessage(m_channel.ID, m_channel.lastMessageID);
                }
                Application::Get()->get_discord_client()->get_cache().set_last_seen_message_for_channel(m_channel.ID, m_last_message_id);

                for (auto it = messages.rbegin(); it != messages.rend(); ++it)
                {
                    auto message = *it;
                    if (!message.content.empty() || !message.attachments.empty())
                    {
                        m_pending_messages.push_back(message);
                    }
                }
            }
            catch (const SleepyDiscord::ErrorCode& error)
            {
                g_warning("MessageView::set_channel - Error: %d while retrieving messages", error);
            }

            return nullptr;
        },
        [this](void*, bool, util::SimpleThread*)
        {
            m_add_messages_timeout_connection = Glib::signal_timeout().connect([this]()
            {
                if (m_loading_widget)
                {
                    m_loading_widget->set_size_request(-1, m_loading_widget->get_height());
                    m_loading_widget->set_child_visible(false);
                }

                if (!m_pending_messages.empty())
                {
                    SleepyDiscord::Message* previous_message = m_pending_messages.size() > 1 ? &(*(m_pending_messages.end() - 2)) : nullptr;
                    auto message_objects = prepare_message_data(m_pending_messages.back(), previous_message);
                    m_pending_messages.pop_back();

                    for (auto it = message_objects.rbegin(); it != message_objects.rend(); ++it)
                    {
                        auto message_widget = new ui::MessageWidget();
                        m_messages_box->insert_child_after(*message_widget, *m_loading_widget);
                        message_widget->init(*it);
                    }
                }

                bool has_available_messages =  m_pending_messages.size() > 0;
                bool should_continue = m_messages_box->get_allocation().get_y() > (-m_scrolled_window->get_height() * 2);
                if (!should_continue)
                {
                    m_loading_widget->set_child_visible(true);
                }
                else if (!has_available_messages)
                {
                    get_messages();
                }
                return should_continue && has_available_messages;
            }, 100);

            Application::Get()->get_message_bar()->set_sensitive(true);
            m_messages_thread = nullptr;
        });
    }

    void MessageView::on_discord_client_ready()
    {
        if (!m_new_message_connection.connected())
        {
            m_new_message_connection = Application::Get()->get_discord_client()->signal_new_message().connect([this](const SleepyDiscord::Message& message)
            {
                if (get_channel().ID == message.channelID)
                {
                    if (!message.content.empty() || !message.attachments.empty())
                    {
                        const SleepyDiscord::Message* previous_message = nullptr;
                        if (m_messages_box->get_last_child())
                        {
                            previous_message = &(static_cast<MessageWidget*>(m_messages_box->get_last_child())->get_message());
                        }
                        auto message_objects = prepare_message_data(message, previous_message);

                        for (const auto& message_obj : message_objects)
                        {
                            auto message_widget = new ui::MessageWidget();
                            m_messages_box->append(*message_widget);
                            message_widget->init(message_obj);
                        }
                    }
                }
            });
        }
    }

    void MessageView::create_header_bar()
    {
        m_header_bar = (GtkHeaderBar*)adw_header_bar_new();

        m_title = new Gtk::Label();
        m_title->add_css_class("title-widget");
        adw_header_bar_set_title_widget((AdwHeaderBar*)m_header_bar, (GtkWidget*)m_title->gobj());

        gtk_box_prepend(m_child->gobj(), (GtkWidget*)m_header_bar);

        add_flap_button(false);
    }

    void MessageView::on_add_reaction(const SleepyDiscord::Snowflake<SleepyDiscord::Message>& message, const SleepyDiscord::Emoji& emoji, bool me)
    {
        for (MessageWidget* message_widget = static_cast<MessageWidget*>(m_messages_box->get_first_child());
                message_widget != nullptr;
                message_widget = static_cast<MessageWidget*>(message_widget->get_next_sibling()))
        {
            if (message_widget->get_message().ID == message)
            {
                message_widget->add_reaction(emoji, me);
            }
        }
    }

    void MessageView::on_delete_reaction(const SleepyDiscord::Snowflake<SleepyDiscord::Message>& message, const SleepyDiscord::Emoji& emoji, bool me)
    {
        for (MessageWidget* message_widget = static_cast<MessageWidget*>(m_messages_box->get_first_child());
             message_widget != nullptr;
             message_widget = static_cast<MessageWidget*>(message_widget->get_next_sibling()))
        {
            if (message_widget->get_message().ID == message)
            {
                message_widget->delete_reaction(emoji, me);
            }
        }
    }
}
