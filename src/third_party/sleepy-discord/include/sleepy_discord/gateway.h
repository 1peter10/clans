#pragma once
#include <ctime>
#include <array>
#include "discord_object_interface.h"
#include "user.h"
#include "channel.h"
#include "server.h"

namespace SleepyDiscord {
	enum Status {
		statusError = 0,
		online         ,
		doNotDisturb   ,
		idle           ,
		invisible      ,
		offline        
	};

	enum GameType {
		Playing = 0,  //calling this Game causes issues
		Streaming = 1
	};

	struct Game : public DiscordObject {
		std::string name = "";
		GameType type;
		std::string url = ""; //used when type is Streaming
	};

    struct MuteConfig
    {
        MuteConfig() = default;
        MuteConfig(const json::Value & rawJSON);
        MuteConfig(const nonstd::string_view & rawJSON);

        int selected_time_window;
        std::string end_time;

        JSONStructStart
            std::make_tuple(
                    json::pair(&MuteConfig::selected_time_window, "selected_time_window", json::REQUIRIED_FIELD),
                    json::pair(&MuteConfig::end_time, "end_time", json::REQUIRIED_FIELD)
            );
        JSONStructEnd
    };

    struct ChannelOverride
    {
        ChannelOverride() = default;
        ChannelOverride(const json::Value & rawJSON);
        ChannelOverride(const nonstd::string_view & rawJSON);

        bool muted;
        MuteConfig mute_config;
        int message_notifications;
        bool collapsed;
        Snowflake<Channel> channel_id;

        JSONStructStart
        std::make_tuple(
                json::pair(&ChannelOverride::muted, "muted", json::REQUIRIED_FIELD),
                json::pair(&ChannelOverride::mute_config, "mute_config", json::REQUIRIED_FIELD),
                json::pair(&ChannelOverride::message_notifications, "message_notifications", json::REQUIRIED_FIELD),
                json::pair(&ChannelOverride::collapsed, "collapsed", json::REQUIRIED_FIELD),
                json::pair(&ChannelOverride::channel_id, "channel_id", json::REQUIRIED_FIELD)
        );
        JSONStructEnd
    };

    struct UserGuildSettings
    {
    public:
        UserGuildSettings() = default;
        UserGuildSettings(const json::Value & rawJSON);
        UserGuildSettings(const nonstd::string_view & rawJSON);

        bool suppress_roles;
        bool suppress_everyone;
        int notify_highlights;
        bool muted;
        bool mute_scheduled_events;
        MuteConfig mute_config;
        bool mobile_push;
        int message_notifications;
        bool hide_muted_channels;
        Snowflake<Server> guild_id;
        int flags;
        std::list<ChannelOverride> channel_overrides;

        JSONStructStart
            std::make_tuple(
                    json::pair(&UserGuildSettings::suppress_roles, "suppress_roles", json::REQUIRIED_FIELD),
                    json::pair(&UserGuildSettings::suppress_everyone, "suppress_everyone", json::REQUIRIED_FIELD),
                    json::pair(&UserGuildSettings::notify_highlights, "notify_highlights", json::REQUIRIED_FIELD),
                    json::pair(&UserGuildSettings::muted, "muted", json::REQUIRIED_FIELD),
                    json::pair(&UserGuildSettings::mute_scheduled_events, "mute_scheduled_events", json::REQUIRIED_FIELD),
                    json::pair(&UserGuildSettings::mute_config, "mute_config", json::REQUIRIED_FIELD),
                    json::pair(&UserGuildSettings::mobile_push, "mobile_push", json::REQUIRIED_FIELD),
                    json::pair(&UserGuildSettings::message_notifications, "message_notifications", json::REQUIRIED_FIELD),
                    json::pair(&UserGuildSettings::hide_muted_channels, "hide_muted_channels", json::REQUIRIED_FIELD),
                    json::pair(&UserGuildSettings::guild_id, "guild_id", json::REQUIRIED_FIELD),
                    json::pair(&UserGuildSettings::flags, "flags", json::REQUIRIED_FIELD),
                    json::pair<json::ContainerTypeHelper>(&UserGuildSettings::channel_overrides, "channel_overrides", json::REQUIRIED_FIELD)
            );
        JSONStructEnd
    };

    struct ReadState
    {
    public:
        ReadState() = default;
        ReadState(const json::Value & rawJSON);
        ReadState(const nonstd::string_view & rawJSON);

        int mention_count;
        std::string last_pin_timestamp;
        Snowflake<Message> last_message_id;
        Snowflake<Channel> id;

        JSONStructStart
                    std::make_tuple(
                            json::pair                           (&ReadState::mention_count             , "mention_count"               , json::REQUIRIED_FIELD),
                            json::pair                           (&ReadState::last_pin_timestamp           , "last_pin_timestamp"            , json::REQUIRIED_FIELD),
                            json::pair                          (&ReadState::last_message_id, "last_message_id", json::REQUIRIED_FIELD),
                            json::pair                          (&ReadState::id        , "id"          , json::REQUIRIED_FIELD)
                    );
        JSONStructEnd
    };

	struct Ready : public DiscordObject {
	public:
		Ready() = default;
		//Ready(const std::string * rawJSON);
		Ready(const json::Value & rawJSON);
		Ready(const nonstd::string_view & rawJSON);
		//Ready(const json::Values values);
		int v;	//gateway protocol version
		User user;
		std::list<Channel> privateChannels;
		std::list<Server> servers;
		std::string sessionID;
		//std::vector<std::string> trace;
		std::array<int, 2> shard = { {0, 1} };
        std::list<ReadState> read_state;
        std::list<UserGuildSettings> user_guild_settings;

		JSONStructStart
			std::make_tuple(
				json::pair                           (&Ready::v              , "v"               , json::REQUIRIED_FIELD),
				json::pair                           (&Ready::user           , "user"            , json::REQUIRIED_FIELD),
				json::pair<json::ContainerTypeHelper>(&Ready::privateChannels, "private_channels", json::REQUIRIED_FIELD),
				json::pair<json::ContainerTypeHelper>(&Ready::servers        , "guilds"          , json::REQUIRIED_FIELD),
				json::pair                           (&Ready::sessionID      , "session_id"      , json::REQUIRIED_FIELD),
				//This doesn't work anymore
				//json::pair(&Ready::trace          , json::toArray<std::string>      , "_trace"          , json::REQUIRIED_FIELD),
				json::pair<json::StdArrayTypeHelper >(&Ready::shard          , "shard"           , json::OPTIONAL_FIELD ),
                json::pair<json::ContainerTypeHelper>(&Ready::read_state     , "read_state"      , json::OPTIONAL_FIELD),
                json::pair<json::ContainerTypeHelper>(&Ready::user_guild_settings, "user_guild_settings", json::REQUIRIED_FIELD)
			);
		JSONStructEnd
	};

	template<class Type>
	struct ActivityTimestampTypeHelper {
		using TypeHelper = json::PrimitiveTypeHelper<Type>;
		static inline Type toType(const json::Value& value) {
			//For some reason Discord sends a string sometimes
			//instead of an int
			return value.IsString() ?
				static_cast<Type>(
					std::stoll(
						std::string(value.GetString(),
						value.GetStringLength())
					)
				)
				:
				TypeHelper::toType(value);
		}
		static inline bool empty(const Type& value) {
			return TypeHelper::empty(value);
		}
		static inline json::Value fromType(const Type& value, json::Value::AllocatorType& allocator) {
			return TypeHelper::fromType(value, allocator);
		}
	};

	struct ActivityTimestamp : public DiscordObject {
	public:
		ActivityTimestamp() = default;
		~ActivityTimestamp() = default;
		ActivityTimestamp(const json::Value & json);
		ActivityTimestamp(const nonstd::string_view & json);
		Time start;
		Time end;

		JSONStructStart
			std::make_tuple(
				json::pair<ActivityTimestampTypeHelper>(&ActivityTimestamp::start, "start", json::OPTIONAL_FIELD),
				json::pair<ActivityTimestampTypeHelper>(&ActivityTimestamp::end  , "end"  , json::OPTIONAL_FIELD)
			);
		JSONStructEnd
	};

	struct ActivityParty : public DiscordObject {
	public:
		ActivityParty() = default;
		~ActivityParty() = default;
		ActivityParty(const json::Value & json);
		ActivityParty(const nonstd::string_view & json);
		std::string ID;
		std::array<int64_t, 2> size;
		int64_t& currentSize = size[0];
		int64_t& maxSize = size[1];

		JSONStructStart
			std::make_tuple(
				json::pair                          (&ActivityParty::ID  , "id"  , json::OPTIONAL_FIELD),
				json::pair<json::StdArrayTypeHelper>(&ActivityParty::size, "size", json::OPTIONAL_FIELD)
			);
		JSONStructEnd
	};

	struct ActivityAssets : public DiscordObject {
	public:
		ActivityAssets() = default;
		~ActivityAssets() = default;
		ActivityAssets(const json::Value & json);
		ActivityAssets(const nonstd::string_view & json);
		std::string largeImage;
		std::string largeText;
		std::string smallImage;
		std::string smallText;

		JSONStructStart
			std::make_tuple(
				json::pair(&ActivityAssets::largeImage, "large_image", json::OPTIONAL_FIELD),
				json::pair(&ActivityAssets::largeText , "large_text" , json::OPTIONAL_FIELD),
				json::pair(&ActivityAssets::smallImage, "small_image", json::OPTIONAL_FIELD),
				json::pair(&ActivityAssets::smallText , "small_text" , json::OPTIONAL_FIELD)
			);
		JSONStructEnd
	};

	struct ActivitySecrets : public DiscordObject {
	public:
		ActivitySecrets() = default;
		~ActivitySecrets() = default;
		ActivitySecrets(const json::Value & json);
		ActivitySecrets(const nonstd::string_view & json);
		std::string join;
		std::string spectate;
		std::string match;

		JSONStructStart
			std::make_tuple(
				json::pair(&ActivitySecrets::join    , "join"    , json::OPTIONAL_FIELD),
				json::pair(&ActivitySecrets::spectate, "spectate", json::OPTIONAL_FIELD),
				json::pair(&ActivitySecrets::match   , "match"   , json::OPTIONAL_FIELD)
			);
		JSONStructEnd
	};

	//This is here for the snowflake
	struct Application {};

	struct Activity : public DiscordObject {
	public:
		Activity() = default;
		~Activity() = default;
		Activity(const json::Value & json);
		Activity(const nonstd::string_view & json);
		std::string name;
		enum ActivityType {
			ACTIVITY_TYPE_NONE = -1,
			GAME               = 0,
			STREAMING          = 1,
			LISTENING          = 2,
		} type = ACTIVITY_TYPE_NONE;
		std::string url;
		ActivityTimestamp timestamps;
		Snowflake<Application> applicationID;
		std::string details;
		std::string state;
		//ActivityParty party;
		ActivityAssets assets;
		ActivitySecrets secrets;
		bool instance;
		enum ActivityFlags {
			NONE         = 0 << 0,
			INSTANCE     = 1 << 0,
			JOIN         = 1 << 1,
			SPECTATE     = 1 << 2,
			JOIN_REQUEST = 1 << 3,
			SYNC         = 1 << 4,
			PLAY         = 1 << 5
		} flags = NONE;

		JSONStructStart
			std::make_tuple(
				json::pair                      (&Activity::name         , "name"          , json::REQUIRIED_FIELD        ),
				json::pair<json::EnumTypeHelper>(&Activity::type         , "type"          , json::REQUIRIED_FIELD        ),
				json::pair                      (&Activity::url          , "url"           , json::OPTIONAL_NULLABLE_FIELD),
				json::pair                      (&Activity::timestamps   , "timestamps"    , json::OPTIONAL_FIELD         ),
				json::pair                      (&Activity::applicationID, "application_id", json::OPTIONAL_FIELD         ),
				json::pair                      (&Activity::details      , "details"       , json::OPTIONAL_NULLABLE_FIELD),
				json::pair                      (&Activity::state        , "state"         , json::OPTIONAL_NULLABLE_FIELD),
				//json::pair                      (&Activity::party        , "party"         , json::OPTIONAL_FIELD         ),
				json::pair                      (&Activity::assets       , "assets"        , json::OPTIONAL_FIELD         ),
				json::pair                      (&Activity::secrets      , "secrets"       , json::OPTIONAL_FIELD         ),
				json::pair                      (&Activity::instance     , "instance"      , json::OPTIONAL_FIELD         ),
				json::pair<json::EnumTypeHelper>(&Activity::flags        , "flags"         , json::OPTIONAL_FIELD         )
			);
		JSONStructEnd
	};

	template<>
	struct GetDefault<Activity::ActivityType> {
		static inline const Activity::ActivityType get() {
			return Activity::ActivityType::ACTIVITY_TYPE_NONE;
		}
	};

	struct PresenceUpdate : public DiscordObject {
	public:
		PresenceUpdate() = default;
		~PresenceUpdate() = default;
		PresenceUpdate(const json::Value & json);
		PresenceUpdate(const nonstd::string_view & json);
		User user;
		std::vector<Snowflake<Role>> roleIDs;
		Activity currentActivity;
		Snowflake<Server> serverID;
		std::string status;
		std::vector<Activity> activities;

		JSONStructStart
			std::make_tuple(
				json::pair                           (&PresenceUpdate::user           , "user"      , json::REQUIRIED_FIELD),
				json::pair<json::ContainerTypeHelper>(&PresenceUpdate::roleIDs        , "roles"     , json::REQUIRIED_FIELD),
				json::pair                           (&PresenceUpdate::currentActivity, "game"      , json::NULLABLE_FIELD ),
				json::pair                           (&PresenceUpdate::serverID       , "guild_id"  , json::OPTIONAL_FIELD ),
				json::pair                           (&PresenceUpdate::status         , "status"    , json::REQUIRIED_FIELD),
				json::pair<json::ContainerTypeHelper>(&PresenceUpdate::activities     , "activities", json::REQUIRIED_FIELD)
			);
		JSONStructEnd
	};

	struct ServerMembersChunk {
		ServerMembersChunk() = default;
		ServerMembersChunk(const json::Value& json);
		ServerMembersChunk(const nonstd::string_view & json);
		Snowflake<Server> serverID;
        std::vector<ServerMember> members;
		int chunkIndex;
		int chunkCount;
		std::vector<PresenceUpdate> presences;
		std::string nonce;

		JSONStructStart
			std::make_tuple(
				json::pair                           (&ServerMembersChunk::serverID  , "guild_id"   , json::REQUIRIED_FIELD),
				json::pair<json::ContainerTypeHelper>(&ServerMembersChunk::members   , "members"    , json::REQUIRIED_FIELD),
				json::pair                           (&ServerMembersChunk::chunkIndex, "chunk_index", json::REQUIRIED_FIELD),
				json::pair                           (&ServerMembersChunk::chunkCount, "chunk_count", json::REQUIRIED_FIELD),
				json::pair<json::ContainerTypeHelper>(&ServerMembersChunk::presences , "presences"  , json::OPTIONAL_FIELD ),
				json::pair                           (&ServerMembersChunk::nonce     , "nonce"      , json::OPTIONAL_FIELD )
			);
		JSONStructEnd
	};
}