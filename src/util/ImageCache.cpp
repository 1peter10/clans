#include <filesystem>
#include "ImageCache.h"
#include "ImageDownloader.h"
#include "Helpers.h"

namespace util
{
    ImageCache* ImageCache::s_Instance = nullptr;

    ImageCache* ImageCache::get()
    {
        if (!s_Instance)
        {
            s_Instance = new ImageCache();
        }

        return s_Instance;
    }

    void ImageCache::get_image(const std::string& url, const std::function<void(const std::string&)>& on_ready, CancellationToken* cancellation_token)
    {
        bool downloader_exists = m_image_downloaders.count(url) > 0;
        bool downloader_active = downloader_exists && !m_image_downloaders[url]->is_completed();

        std::string disk_path = get_image_disk_path(url);
        if (!downloader_active && std::filesystem::exists(disk_path))
        {
            on_ready(disk_path);
            return;
        }

        m_mutex.lock();
        if (downloader_exists)
        {
            m_image_downloaders[url]->add_callback(on_ready, cancellation_token);
        }
        else
        {
            m_image_downloaders[url] = new util::ImageDownloader();
            m_image_downloaders[url]->download_image_async(url, [this, url, on_ready](const std::string&)
            {
                m_mutex.lock();
                m_image_downloaders.erase(url);
                m_mutex.unlock();
            }, nullptr);
            m_image_downloaders[url]->add_callback(on_ready, cancellation_token);
        }
        m_mutex.unlock();
    }

    std::string ImageCache::get_image_disk_path(const std::string& url)
    {
        if (url.empty())
        {
            return "";
        }

        auto image_url_split = util::Helpers::split_string(url, '/');
        std::string file_name = image_url_split[image_url_split.size() - 1];

        if (file_name.find('?') != std::string::npos)
        {
            file_name.resize(file_name.find('?'));
        }

        auto it = file_name.find_last_of('.');
        std::string extension = file_name.substr(it);
        file_name = file_name.substr(0, it);
        file_name.erase(std::remove(file_name.begin(), file_name.end(), '.'), file_name.end());
        file_name = util::Helpers::str_format("%s/clans/%s-%s%s", Glib::get_user_cache_dir().c_str(), image_url_split[image_url_split.size() - 2].c_str(), file_name.c_str(), extension.c_str());

        return file_name;
    }
}