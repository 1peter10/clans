#pragma once

namespace util
{
    class DateTime
    {
    public:
        DateTime(int year, int month, int day, int hour, int minute, float second);
        DateTime(int year, int month, int day, int hour, int minute);
        DateTime(int year, int month, int day, int hour);
        DateTime(int year, int month, int day);
        DateTime(tm* t);

        DateTime to_local() const;
        tm to_tm() const;
        std::string to_str() const;

        static DateTime now();
        uint64_t get_epoch_seconds() const;

        int get_year() const { return m_year; }
        int get_month() const { return m_month; }
        int get_day() const { return m_day; }
        int get_hour() const { return m_hour; }
        int get_minute() const { return m_minute; }
        int get_second() const { return m_second; }

        bool operator> (const DateTime& rhs) const;
        bool operator< (const DateTime& rhs) const;
        bool operator>= (const DateTime& rhs) const;
        bool operator<= (const DateTime& rhs) const;

    private:
        int m_year = 1970;
        int m_month;
        int m_day;
        int m_hour = 0;
        int m_minute = 0;
        float m_second = 0;
    };
}