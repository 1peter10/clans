#include "ImageDownloader.h"
#include "SimpleThread.h"
#include "ThreadWorker.h"
#include "Helpers.h"
#include <filesystem>
#include <utility>
#include <curl/curl.h>
#include "ImageCache.h"

namespace util
{
    void ImageDownloader::download_image_async(const std::string& image_url, std::function<void(const std::string&)> on_finished, CancellationToken* cancellation_token)
    {
        if (m_image_download_thread)
        {
            g_warning("Attempting to download an image with an active download thread. aborting.");
            return;
        }

        m_image_url = image_url;

        m_mutex.lock();
        JobCallback job_cb { cancellation_token, std::move(on_finished) };
        m_on_finished.emplace_back(job_cb);
        m_mutex.unlock();

        if (m_image_url.empty())
        {
            return;
        }

        m_image_download_thread = new util::SimpleThread([this](util::ThreadWorker* worker)
        {
            download_image_internal(worker);
            return nullptr;
        },
        [this](void*, bool cancelled, util::SimpleThread*)
        {
            if (!cancelled)
            {
                m_image_download_thread = nullptr;

                m_mutex.lock();
                m_completed = true;
                for (const auto& cb : m_on_finished)
                {
                    if (!cb.m_cancellation_token || !cb.m_cancellation_token->is_cancelled())
                    {
                        cb.m_cb(ImageCache::get_image_disk_path(m_image_url));
                    }

                    if (cb.m_cancellation_token)
                    {
                        delete cb.m_cancellation_token;
                    }

                }
                m_mutex.unlock();
            }
        });
    }

    void ImageDownloader::add_callback(const std::function<void(const std::string&)>& cb, CancellationToken* cancellation_token)
    {
        m_mutex.lock();
        JobCallback job_cb { cancellation_token, cb };
        m_on_finished.emplace_back(job_cb);
        m_mutex.unlock();
    }

    void ImageDownloader::download_image_internal(util::ThreadWorker* worker)
    {
        if (worker->is_cancelled())
        {
            return;
        }

        std::string filePath = ImageCache::get_image_disk_path(m_image_url);

        if (worker->is_cancelled())
        {
            return;
        }

        if (!std::filesystem::exists(filePath))
        {
            do_download(m_image_url, filePath);
        }

        if (worker->is_cancelled())
        {
            return;
        }
    }

    void ImageDownloader::cancel()
    {
        if (m_image_download_thread)
        {
            m_image_download_thread->cancel();
            m_image_download_thread = nullptr;
        }
    }

    size_t image_download_callback(void* ptr, size_t size, size_t nmemb, void* userdata)
    {
        FILE* stream = (FILE*) userdata;
        if (!stream)
        {
            return 0;
        }

        size_t written = fwrite((FILE*) ptr, size, nmemb, stream);
        return written;
    }

    void ImageDownloader::do_download(const std::string& url, const std::string& out_file_name)
    {
        errno = 0;
        FILE* file = fopen(out_file_name.c_str(), "wb");
        if (!file)
        {
            g_warning("failed to open file for writing: %s with error %d", out_file_name.c_str(), errno);
        }

        CURL* curl = curl_easy_init();
        curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, file);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, image_download_callback);
        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_easy_setopt(curl, CURLOPT_USERAGENT, "Clans/1.0");
        curl_easy_setopt(curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        CURLcode rc = curl_easy_perform(curl);
        curl_easy_cleanup(curl);

        fclose(file);
    }

    bool ImageDownloader::is_completed()
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        return m_completed;
    }
}
