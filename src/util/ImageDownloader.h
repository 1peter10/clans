#pragma once

#include "CancellationToken.h"

namespace util
{
    class SimpleThread;
    class ThreadWorker;
    class ImageDownloader
    {
    public:
        void download_image_async(const std::string& image_url, std::function<void(const std::string&)> on_finished, CancellationToken* cancellation_token);
        void cancel();
        void add_callback(const std::function<void(const std::string&)>& cb, CancellationToken* cancellation_token);
        bool is_completed();

    private:
        void download_image_internal(util::ThreadWorker* worker);
        void do_download(const std::string& url, const std::string& out_file_name);

        std::string m_image_url;

        std::mutex m_mutex;

        struct JobCallback
        {
            CancellationToken* m_cancellation_token;
            std::function<void(std::string)> m_cb;
        };

        std::vector<JobCallback> m_on_finished;
        util::SimpleThread* m_image_download_thread = nullptr;
        bool m_completed = false;
    };
}
