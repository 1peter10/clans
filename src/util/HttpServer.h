#pragma once

struct MHD_Daemon;
namespace util
{
    class HttpServer
    {
    public:
        HttpServer();
        void run(int port);
        void stop();

        void set_on_arg_callback(std::function<void(std::string, std::string)> cb);
        void on_arg_recieved(std::string key, std::string value);

        struct RunServerArgs
        {
            const char* m_response;
            HttpServer* m_server;
        };

        struct IterateArgumentsArgs
        {
            HttpServer* m_server;
        };
    private:

        RunServerArgs* m_run_server_args;
        MHD_Daemon* m_mhd_daemon;
        std::function<void(std::string, std::string)> m_on_argument_callback;
    };
}
