#include <sstream>
#include <chrono>
#include <iomanip>
#include "Helpers.h"
#include <regex>

namespace util
{
    std::vector<std::string> Helpers::split_string(const std::string& str, char delim)
    {
        std::vector<std::string> ret;
        std::stringstream ss(str);
        std::string token;
        while (std::getline(ss, token, delim))
        {
            ret.push_back(token);
        }

        return ret;
    }

    void Helpers::open_browser(const std::string& url)
    {
        std::ostringstream oss;
        oss << "xdg-open \"" << url << "\"";
        int ret = system(oss.str().c_str());
        if (ret != 0)
        {
            g_warning("Error: %i while running xdg-open", ret);
        }
    }

    std::string Helpers::unescape_html(const std::string& data)
    {
        std::string ret = data;

        auto it = ret.find("&quot;");
        while(it != std::string::npos)
        {
            ret = ret.replace(it, 6, "\"");
            it = ret.find("&quot;");
        }

        it = ret.find("&amp;");
        while(it != std::string::npos)
        {
            ret = ret.replace(it, 5, "&");
            it = ret.find("&amp;");
        }

        it = ret.find("&lt;");
        while(it != std::string::npos)
        {
            ret = ret.replace(it, 4, "<");
            it = ret.find("&lt;");
        }

        it = ret.find("&gt;");
        while(it != std::string::npos)
        {
            ret = ret.replace(it, 4, ">");
            it = ret.find("&gt;");
        }

        return ret;
    }

    uint64_t Helpers::get_seconds_since_epoch()
    {
        const auto now = std::chrono::system_clock::now();
        const auto epoch = now.time_since_epoch();
        const auto seconds = std::chrono::duration_cast<std::chrono::seconds>(epoch);
        return seconds.count();
    }

    std::string Helpers::timestamp_to_date_string(uint64_t time_stamp)
    {
        std::time_t temp = time_stamp;
        std::tm* t = std::localtime(&temp);
        std::stringstream ss;
        ss << std::put_time(t, "%d/%m/%Y");
        return ss.str();
    }

    std::string Helpers::timestamp_to_time_and_date_string(uint64_t time_stamp)
    {
        std::time_t temp = time_stamp;
        std::tm* t = std::localtime(&temp);
        std::stringstream ss;
        ss << std::put_time(t, "%H:%M - %d/%m/%Y");
        return ss.str();
    }

    void Helpers::copy_to_clipboard(const std::string& str)
    {
        Glib::RefPtr<Gdk::Clipboard> clipboard = Gdk::Display::get_default()->get_clipboard();
        if (!clipboard)
        {
            return;
        }

        clipboard->set_text(str);
    }

    unsigned int Helpers::hex_str_to_uint(std::string hex)
    {
        unsigned int ret;

        hex.erase(0, 1); //remove the #

        std::stringstream ss;
        ss << std::hex << hex;
        ss >> ret;

        return ret;
    }

    std::vector<std::string> Helpers::split_string(const std::string& str, const std::string& delim)
    {
        std::vector<std::string> ret;
        std::string input = str;

        size_t pos;
        while ((pos = input.find(delim)) != std::string::npos)
        {
            ret.push_back(input.substr(0, pos));
            input.erase(0,pos + delim.length());
        }

        if (!input.empty())
        {
            ret.push_back(input);
        }
        return ret;
    }

    std::string Helpers::str_format(const std::string& fmt, ...)
    {
        char buf[256];

        va_list args;
        va_start(args, fmt);
        const auto r = std::vsnprintf(buf, sizeof buf, fmt.c_str(), args);
        va_end(args);

        if (r < 0)
        {
            g_warning("failed to format string.");
            return std::string();
        }

        const size_t len = r;
        if (len < sizeof buf)
        {
            return { buf, len };
        }

        std::string s(len, '\0');
        va_start(args, fmt);
        std::vsnprintf(s.data(), len + 1, fmt.c_str(), args);
        va_end(args);

        return s;
    }

    std::string Helpers::get_file_extension(const std::string& filePath)
    {
        size_t it = 0;
        size_t extensionIndex = 0;
        while (it != std::string::npos)
        {
            extensionIndex = it + 1;
            it = filePath.find('.', it + 1);
        }

        std::string fileExtension = filePath.substr(extensionIndex, filePath.size() - extensionIndex);

        return fileExtension;
    }

    std::string Helpers::get_mime_type(const std::string& filePath)
    {
        static std::unordered_map<std::string, std::string> mimeTypeMap =
                {
                        { "png",  "image/png" },
                        { "mov",  "video/quicktime" },
                        { "mp4",  "video/mp4" },
                        { "jpg",  "image/jpeg" },
                        { "jpeg", "image/jpeg" },
                        { "gif",  "image/gif" },
                };

        std::string extension = get_file_extension(filePath);
        ASSERT(mimeTypeMap.find(extension) != mimeTypeMap.end(), "Helpers::get_mime_type - Unhandled file type %s", extension.c_str());
        return mimeTypeMap[extension];
    }

    std::string Helpers::get_filename(const std::string& filePath)
    {
        size_t it = 0;
        size_t slashIndex = 0;
        while (it != std::string::npos)
        {
            slashIndex = it + 1;
            it = filePath.find('/', it + 1);
        }

        std::string fileExtension = filePath.substr(slashIndex, filePath.size() - slashIndex);

        return fileExtension;
    }

    std::string Helpers::str_replace(std::string str, const std::string& find, const std::string& replace)
    {
        size_t it = str.find(find);
        while (it != std::string::npos)
        {
            str.replace(it, find.length(), replace);
            it = str.find(find);
        }

        return str;
    }

    Json::Value Helpers::get_json_from_string(const std::string& json_str)
    {
        Json::CharReaderBuilder builder;
        builder["collectComments"] = false;
        Json::Value jsonValue;
        JSONCPP_STRING errs;
        Json::CharReader* charReader = builder.newCharReader();

        if (!charReader->parse(json_str.data(), json_str.data() + json_str.size(), &jsonValue, &errs))
        {
            g_warning("Json::CharReader error: %s", errs.c_str());
        }

        delete charReader;
        return jsonValue;
    }

    size_t find_whitespace(const std::string& str)
    {
        for (size_t i = 0; i < str.length(); ++i)
        {
            if (std::isspace(str[i]))
            {
                return i - 1;
            }
        }

        return std::string::npos;
    }

    std::vector<std::string> Helpers::extract_urls_from_str(const std::string& str)
    {
        std::vector<std::string> ret;

        auto it = str.find("http://");
        while (it != std::string::npos)
        {
            auto end_it = find_whitespace(str);
            if (end_it != std::string::npos)
            {
                ret.push_back(str.substr(it, end_it - it + 1));
            }
            else
            {
                ret.push_back(str.substr(it, str.size() - it + 1));
                break;
            }

            it = str.find("http://", it + 1);
        }

        it = str.find("https://");
        while (it != std::string::npos)
        {
            auto end_it = find_whitespace(str);
            if (end_it != std::string::npos)
            {
                ret.push_back(str.substr(it, end_it - it + 1));
            }
            else
            {
                ret.push_back(str.substr(it, str.size() - it + 1));
                break;
            }

            it = str.find("https://", it + 1);
        }

        return ret;
    }

    void Helpers::handle_link(const std::string& url)
    {
        std::stringstream ss;
        ss << "xdg-open " << url;
        int ret = system(ss.str().c_str());
        if (ret != 0)
        {
            g_warning("Error: %i while opening link", ret);
        }
    }

    DateTime Helpers::discord_time_str_to_datetime(const std::string& str)
    {
        auto it = str.find('T');
        std::string date = str.substr(0, it);
        std::string time = str.substr(it + 1, str.find('+') - (it + 1));

        auto date_split = util::Helpers::split_string(date, '-');
        int year = stoi(date_split[0]);
        int month = stoi(date_split[1]);
        int day = stoi(date_split[2]);

        auto time_split = util::Helpers::split_string(time, ':');
        int hour = stoi(time_split[0]);
        int minute = stoi(time_split[1]);
        float second = stof(time_split[2]);

        return DateTime(year, month, day, hour, minute, second);
    }
}
