#include "PasswordManager.h"

namespace util
{
    static void on_password_stored(GObject*, GAsyncResult* result, gpointer)
    {
        GError* error = NULL;
        secret_password_store_finish(result, &error);

        if (error != NULL)
        {
            g_warning("PasswordManager - Error storing password: %s", error->message);
            g_error_free(error);
        }
    }

    static const SecretSchema* get_schema()
    {
        static const SecretSchema schema = {
                "io.gitlab.caveman250.clans.Password", SECRET_SCHEMA_NONE,
                {
                        {  "type", SECRET_SCHEMA_ATTRIBUTE_STRING },
                        { "NULL", SECRET_SCHEMA_ATTRIBUTE_STRING },
                },
                0, NULL, NULL, NULL, NULL, NULL, NULL, NULL
        };

        return &schema;
    }

    void PasswordManager::save_token(const std::string& token)
    {
        secret_password_store(get_schema(), SECRET_COLLECTION_DEFAULT, "Clans - Token",
                              token.c_str(), NULL, on_password_stored, NULL,
                              "type", "token",
                              (char*) NULL);
    }

    std::string PasswordManager::load_token()
    {
        GError* error = NULL;
        gchar *password = secret_password_lookup_sync(get_schema(), NULL, &error, "type", "token", (char*)NULL);

        if (error != NULL)
        {
            g_warning("PasswordManager - Error loading token: %s", error->message);
            g_error_free(error);
        }

        if (password == nullptr)
        {
            return "";
        }

        return password;
    }

#define CHECK_ERROR() \
if (!VERIFY(error == nullptr, "%s", error->message))\
{\
    g_error_free(error);\
    error = nullptr;\
}

    static SecretCollection* get_secret_collection(SecretService* service)
    {
        GError* error = nullptr;
        SecretCollection* collection = secret_collection_for_alias_sync(service, "default", SECRET_COLLECTION_LOAD_ITEMS, nullptr, &error);
        CHECK_ERROR()

        return collection;
    }

    bool PasswordManager::ensure_collection_unlocked()
    {
        GError* error = nullptr;
        SecretService* service = secret_service_get_sync(SECRET_SERVICE_LOAD_COLLECTIONS, nullptr, &error);
        CHECK_ERROR()

        SecretCollection* collection = get_secret_collection(service);

        if (!VERIFY(collection, "Could not find default keyring."))
        {
            return false;
        }

        if (secret_collection_get_locked(collection))
        {
            error = nullptr;
            GList* unlocked = nullptr;
            GList* list = g_list_alloc();
            list->data = collection;
            secret_service_unlock_sync(service, list, nullptr, &unlocked, &error);
            CHECK_ERROR()
            g_list_free(list);

            if (VERIFY(unlocked, "Failed to unlock default keyring"))
            {
                SecretCollection* unlockedCollection =  (SecretCollection*)unlocked->data;
                CHECK_ERROR()
                g_list_free(unlocked);
                return unlockedCollection;
            }

            return false;
        }
        else
        {
            return true;
        }
    }
}