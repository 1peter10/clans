#include "SimpleThread.h"

#include <utility>
#include "ThreadWorker.h"

namespace util
{
    SimpleThread::SimpleThread(const std::function<void*(ThreadWorker*)>& task, std::function<void(void*, bool, SimpleThread*)> on_finished)
            : m_on_finished(std::move(on_finished))
    {
        m_main_thread_dispatcher.connect([this]()
       {
           if (m_on_finished != nullptr)
           {
               m_on_finished(m_user_data, m_worker->is_cancelled(), this);
           }

           delete m_worker;
           m_thread->join();
           delete m_thread;
           delete this;
       });

        m_worker = new ThreadWorker();
        m_thread = new std::thread([this, task]()
        {
            m_worker->run([this, task](ThreadWorker* worker)
            {
                m_user_data = task(worker);
            },
            [this](bool cancelled, ThreadWorker*)
            {
                if (!cancelled)
                {
                    m_main_thread_dispatcher.emit();
                }
            });
        });
    }

    void SimpleThread::cancel()
    {
        m_worker->cancel();
    }
}
