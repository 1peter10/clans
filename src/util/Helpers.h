#pragma once
#include "DateTime.h"

namespace util
{
    class Helpers
    {
    public:
        static std::vector<std::string> split_string(const std::string& str, char delim = ' ');
        static void open_browser(const std::string& url);
        static void handle_link(const std::string& url);
        static std::string unescape_html(const std::string& data);
        static uint64_t get_seconds_since_epoch();
        static std::string timestamp_to_date_string(uint64_t time_stamp);
        static std::string timestamp_to_time_and_date_string(uint64_t time_stamp);
        static void copy_to_clipboard(const std::string& str);
        static unsigned int hex_str_to_uint(std::string hex);
        static std::vector<std::string> split_string(const std::string& str, const std::string& delim);
        static std::string str_format(const std::string& fmt, ...);
        static std::string str_replace(std::string str, const std::string& find, const std::string& replace);
        static std::string get_filename(const std::string& filePath);
        static std::string get_file_extension(const std::string& filePath);
        static std::string get_mime_type(const std::string& filePath);
        static Json::Value get_json_from_string(const std::string& json_str);
        static std::vector<std::string> extract_urls_from_str(const std::string& str);
        static DateTime discord_time_str_to_datetime(const std::string& str);

    };
}