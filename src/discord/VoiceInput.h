#pragma once

#include "pulse/pulseaudio.h"

namespace discord
{
    struct VoiceInput : public SleepyDiscord::AudioVectorSource
    {
    public:
        VoiceInput();
        ~VoiceInput() override;
        virtual void read(SleepyDiscord::AudioTransmissionDetails& details, Container& target) override;
        std::vector<uint8_t>& get_pending_audio() { return m_pending_audio; }

    private:
        std::vector<uint8_t> m_pending_audio;
        pa_threaded_mainloop* m_mainloop;
        pa_mainloop_api* m_mainloop_api;
        pa_context* m_context;
        pa_stream* m_stream;
    };
}

