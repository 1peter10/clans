#pragma once

namespace discord
{
    class UserSettings
    {
    public:
        UserSettings(const std::list<SleepyDiscord::UserGuildSettings>& user_guild_settings);
        bool get_channel_muted(SleepyDiscord::Snowflake<SleepyDiscord::Server> server_id, SleepyDiscord::Snowflake<SleepyDiscord::Channel> channel);
        void set_channel_muted(SleepyDiscord::Snowflake<SleepyDiscord::Server> server_id, SleepyDiscord::Snowflake<SleepyDiscord::Channel> channel, bool muted);
    private:

        std::unordered_map<int64_t, bool> m_dm_muted_map;
        std::unordered_map<int64_t, std::unordered_map<int64_t, bool>> m_muted_map;
    };
}
