#include "ChannelStateCache.h"
#include "../util/Helpers.h"

namespace discord
{
    ChannelStateCache::ChannelStateCache()
    {
    }

    void ChannelStateCache::set_last_seen_message_for_channel(SleepyDiscord::Snowflake<SleepyDiscord::Channel> channel, SleepyDiscord::Snowflake<SleepyDiscord::Message> message)
    {
        if (message.empty())
        {
            return;
        }

        m_last_seen_for_channel[channel.number()] = message;
    }

    void ChannelStateCache::set_last_notified_time_for_channel(SleepyDiscord::Snowflake<SleepyDiscord::Channel> channel, SleepyDiscord::Snowflake<SleepyDiscord::Message> message)
    {
        if (message.empty())
        {
            return;
        }

        m_last_notified_for_channel[channel.number()] = message.timestamp().time_since_epoch().count();
        save();
    }

    int64_t ChannelStateCache::get_last_seen_message_for_channel(SleepyDiscord::Snowflake<SleepyDiscord::Channel> channel)
    {
        if (m_last_seen_for_channel.count(channel.number()) > 0)
        {
            return m_last_seen_for_channel[channel.number()].number();
        }

        return -1;
    }

    int64_t ChannelStateCache::get_last_notified_time_for_channel(SleepyDiscord::Snowflake<SleepyDiscord::Channel> channel)
    {
        if (m_last_notified_for_channel.count(channel.number()) > 0)
        {
            return m_last_notified_for_channel[channel.number()];
        }

        return -1;
    }

    std::string get_file_path()
    {
        std::stringstream ss;
        ss << g_get_user_config_dir() << "/clans/channel_state_cache.json";
        return ss.str();
    }

    void ChannelStateCache::load()
    {
        std::ifstream ifstream;
        ifstream.open(get_file_path());
        if (ifstream.is_open())
        {
            std::string lineValue;
            std::stringstream stringValue;
            while (!ifstream.eof())
            {
                ifstream >> lineValue;
                stringValue << lineValue;
            }

            Json::Value data = util::Helpers::get_json_from_string(stringValue.str());

            Json::Value seen_data = data["notified_time"];
            if (!seen_data.isNull())
            {
                for (const auto& member : seen_data.getMemberNames())
                {
                    m_last_notified_for_channel[std::stoll(member)] = seen_data[member].asInt64();
                }
            }
        }
    }

    void ChannelStateCache::save()
    {
        //we only save the notified state
        Json::Value data;
        Json::Value& notified_data = data["notified_time"];
        for (const auto& [channel, last_notified] : m_last_notified_for_channel)
        {
            notified_data[std::to_string(channel)] = last_notified;
        }

        std::ofstream ofstream;
        ofstream.open(get_file_path());


        Json::StreamWriterBuilder builder;
        Json::StreamWriter* writer = builder.newStreamWriter();
        writer->write(data, &ofstream);
        ofstream.close();
    }
}