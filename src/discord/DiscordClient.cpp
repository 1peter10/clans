#include "DiscordClient.h"
#include "Application.h"
#include "ui/SideBar.h"
#include "ui/EmptyMessageView.h"

namespace discord
{
    DiscordClient::~DiscordClient()
    {
        delete m_user_settings;
    }

    bool DiscordClient::is_ready() const
    {
        return m_is_ready;
    }

    void DiscordClient::send_message(const std::string& message, std::vector<std::string> attachments)
    {
        if (message.empty() && attachments.empty())
        {
            g_warning("DiscordClient::send_message - attempting to send an empty message. aborting.");
            return;
        }

        try
        {
            if (attachments.empty())
            {
                SleepyDiscord::DiscordClient::sendMessage(Application::Get()->get_message_view()->get_channel(), message);
            }
            else
            {
                for (unsigned int i = 0; i < attachments.size(); ++i)
                {
                    SleepyDiscord::DiscordClient::uploadFile(Application::Get()->get_message_view()->get_channel(), attachments[i], i == 0 ? message : "");
                }
            }
        }
        catch (const SleepyDiscord::ErrorCode& error)
        {
           // sleepy discord already reports errors. ignore.
        }
    }

    void DiscordClient::onMessage(SleepyDiscord::Message message)
    {
        m_signal_message.emit(message);

        if (message.author.username != s_user)
        {
            if (!m_user_settings->get_channel_muted(message.serverID, message.channelID))
            {
                send_notification_for_channel(message.channelID, &message);
            }
        }

        get_cache().set_last_notified_time_for_channel(message.channelID, message);
    }

    void DiscordClient::onReady(SleepyDiscord::Ready readyData)
    {
        m_is_ready = true;
        m_current_user = readyData.user;
        m_user_settings = new UserSettings(readyData.user_guild_settings);

        //cache servers and channels now
        cache_dm_channels(readyData.privateChannels);
        cache_servers(readyData.servers);
        for (const auto& server : readyData.servers)
        {
            cache_server_channels(server, server.channels);
        }

        for (const auto& server : readyData.servers)
        {
            for (const auto& voice_state : server.voice_states)
            {
                m_voice_states[voice_state.userID.number()] = voice_state.channelID;
            }
        }

        for (const auto& read_state : readyData.read_state)
        {
            m_cache.set_last_seen_message_for_channel(read_state.id, read_state.last_message_id);

            if (!read_state.last_message_id.empty() && read_state.last_message_id.timestamp().time_since_epoch().count() > m_cache.get_last_notified_time_for_channel(read_state.id))
            {
                m_cache.set_last_notified_time_for_channel(read_state.id, read_state.last_message_id);
            }
        }

        Application::Get()->run_on_ui_thread([this]()
        {
            Application::Get()->on_discord_client_ready();
        });
    }

    void DiscordClient::onReaction(SleepyDiscord::Snowflake<SleepyDiscord::User> userID,
                                   SleepyDiscord::Snowflake<SleepyDiscord::Channel> channelID,
                                   SleepyDiscord::Snowflake<SleepyDiscord::Message> messageID,
                                   SleepyDiscord::Emoji emoji)
    {
        BaseDiscordClient::onReaction(userID, channelID, messageID, emoji);

        Application::Get()->get_message_view()->on_add_reaction(messageID, emoji, userID == getID());
    }

    void DiscordClient::onDeleteReaction(SleepyDiscord::Snowflake<SleepyDiscord::User> userID,
                                   SleepyDiscord::Snowflake<SleepyDiscord::Channel> channelID,
                                   SleepyDiscord::Snowflake<SleepyDiscord::Message> messageID,
                                   SleepyDiscord::Emoji emoji)
    {
        BaseDiscordClient::onDeleteReaction(userID, channelID, messageID, emoji);
        Application::Get()->get_message_view()->on_delete_reaction(messageID, emoji, userID == getID());
    }

    SleepyDiscord::Server DiscordClient::get_server(SleepyDiscord::Snowflake<SleepyDiscord::Server> server_id)
    {
        auto it = std::find_if(m_servers.begin(), m_servers.end(), [server_id](const SleepyDiscord::Server& server){ return server.ID == server_id; });
        if (it != m_servers.end())
        {
            return (*it);
        }

        return SleepyDiscord::Server();
    }

    void DiscordClient::cache_servers(std::list<SleepyDiscord::Server> servers)
    {
        m_servers = servers;
    }

    std::list<SleepyDiscord::Server> DiscordClient::get_servers()
    {
        try
        {
            if (m_servers.empty())
            {
                cache_servers(getServers().list());
            }
        }
        catch (const SleepyDiscord::ErrorCode& error)
        {
            g_warning("DiscordClient::get_servers - Error: %d while retrieving servers.", error);
        }

        return m_servers;
    }

    void DiscordClient::cache_server_channels(SleepyDiscord::Server server, std::list<SleepyDiscord::Channel> channels)
    {
        m_channels[server.ID.number()] = channels;
        m_channels[server.ID.number()].sort([](const SleepyDiscord::Channel& a, const SleepyDiscord::Channel& b)
        {
            return a.position < b.position;
        });

        // for some reason the list of channels passed by ready_data doesnt include server ID's
        for (auto& channel : m_channels[server.ID.number()])
        {
            channel.serverID = server.ID;
        }

        Application::Get()->run_on_ui_thread([this, server]()
                                             {
                                                 for (const auto& channel: m_channels[server.ID.number()])
                                                 {
                                                     std::string action_name = util::Helpers::str_format("view-channel-%s", channel.ID.string().c_str());
                                                     Application::Get()->get_gtk_app()->add_action(action_name, [this, channel]()
                                                     {
                                                         view_channel(channel);
                                                     });
                                                 }
                                             });
    }

    std::list<SleepyDiscord::Channel> DiscordClient::get_server_channels(SleepyDiscord::Server server, bool force_refresh)
    {
        if (m_channels[server.ID.number()].empty() || force_refresh)
        {
            try
            {
                cache_server_channels(server, getServerChannels(server));
            }
            catch (const SleepyDiscord::ErrorCode& error)
            {
                g_warning("DiscordClient::get_server_channels - Error: %d while retrieving channels.", error);
            }
        }

        return m_channels[server.ID.number()];
    }

    void DiscordClient::cache_dm_channels(std::list<SleepyDiscord::Channel> channels)
    {
        m_dm_channels = channels;
        Application::Get()->run_on_ui_thread([this]()
                                             {
                                                 for (const auto& channel: m_dm_channels)
                                                 {
                                                     std::string action_name = util::Helpers::str_format("view-channel-%s", channel.ID.string().c_str());
                                                     Application::Get()->get_gtk_app()->add_action(action_name, [this, channel]()
                                                     {
                                                         view_channel(channel);
                                                     });
                                                 }
                                             });
    }

    std::list<SleepyDiscord::Channel> DiscordClient::get_dm_channels(bool force_refresh)
    {
        if (m_dm_channels.empty() || force_refresh)
        {
            try
            {
                cache_dm_channels(getDirectMessageChannels());
            }
            catch (const SleepyDiscord::ErrorCode& error)
            {
                g_warning("DiscordClient::get_dm_channels - Error: %d while retrieving channels.", error);
            }
        }

        return m_dm_channels;
    }

    void DiscordClient::check_for_missed_messages()
    {
        for (const auto& server : m_servers)
        {
            try
            {
                auto channels = get_server_channels(server);
                for (auto& channel: channels)
                {
                    if (channel.type == SleepyDiscord::Channel::ChannelType::SERVER_TEXT ||
                        channel.type == SleepyDiscord::Channel::ChannelType::GUILD_NEWS ||
                        channel.type == SleepyDiscord::Channel::ChannelType::DM ||
                        channel.type == SleepyDiscord::Channel::ChannelType::GROUP_DM)
                    {
                        if (!channel.lastMessageID.empty())
                        {
                            if (has_permission(channel, SleepyDiscord::READ_MESSAGES | SleepyDiscord::VIEW_CHANNEL))
                            {
                                if (channel.lastMessageID.timestamp().time_since_epoch().count() > m_cache.get_last_notified_time_for_channel(channel))
                                {
                                    if (!m_user_settings->get_channel_muted(channel.serverID, channel))
                                    {
                                        auto messages = getMessages(channel.ID, GetMessagesKey::limit, channel.lastMessageID, 1).vector();
                                        for (auto message: messages)
                                        {
                                            send_notification_for_channel(channel, &message);
                                        }
                                    }

                                    get_cache().set_last_notified_time_for_channel(channel.ID, channel.lastMessageID);
                                }

                                if (channel.lastMessageID != m_cache.get_last_seen_message_for_channel(channel))
                                {
                                    if (ui::SideBar* side_bar = Application::Get()->get_side_bar())
                                    {
                                        side_bar->mark_channel_unread(channel);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (const SleepyDiscord::ErrorCode& error)
            {
                g_warning("DiscordClient::check_for_missed_messages - Error: %d", error);
            }
        }
    }

    void DiscordClient::send_notification_for_channel(SleepyDiscord::Snowflake<SleepyDiscord::Channel> channel_id, SleepyDiscord::Message* message)
    {
        SleepyDiscord::Channel channel = getChannel(channel_id);

        std::string message_title;
        std::string message_content;
        if (message)
        {
            message_title = util::Helpers::str_format("%s (#%s)", message->author.username.c_str(), channel.name.c_str());
            message_content = message->content;
        }
        else
        {
            message_title = util::Helpers::str_format("#%s", channel.name.c_str());
            message_content = "New messages";
        }

        auto notification = Gio::Notification::create(message_title);
        notification->set_body(message_content);

        std::stringstream ss;
        std::string action_name = util::Helpers::str_format("app.view-channel-%s", channel.ID.string().c_str());
        notification->set_default_action(action_name);
        Application::Get()->get_gtk_app()->send_notification(util::Helpers::str_format("clans-new-message-%lld",channel_id.number()), notification);
    }

    void DiscordClient::onResumed()
    {
        check_for_missed_messages();
    }

    void DiscordClient::onError(SleepyDiscord::ErrorCode errorCode, const std::string errorMessage)
    {
        if (errorCode == SleepyDiscord::ErrorCode::AUTHENTICATION_FAILED)
        {
            Application::Get()->run_on_ui_thread([]()
            {
                Application::Get()->on_discord_client_login_failed();
            });
        }
    }

    void DiscordClient::view_channel(const SleepyDiscord::Channel& channel)
    {
        Application::Get()->activate_if_needed();
        Application::Get()->close_flap_if_needed();
        Application::Get()->remove_empty_message_view_if_needed();
        Application::Get()->get_message_view()->set_channel(channel);

        Application::Get()->get_side_bar()->mark_channel_read(channel.ID);
    }

    void DiscordClient::onEditVoiceState(SleepyDiscord::VoiceState& state)
    {
        BaseDiscordClient::onEditVoiceState(state);

        m_voice_states[state.userID.number()] = state.channelID;
        Application::Get()->get_side_bar()->update_user_voice_states();
    }

    void DiscordClient::connect_to_voice_channel(SleepyDiscord::Snowflake<SleepyDiscord::Channel> channel)
    {
        m_voice_event_handler = new VoiceEventHandler();
        connectToVoiceChannel(createVoiceContext(channel, m_voice_event_handler));
        m_connected_to_voice_channel = channel;
    }

    void DiscordClient::disconnect_from_voice_channel()
    {
        if (!is_connected_to_voice())
        {
            g_warning("attempting to disconnect form voice when not connected");
            return;
        }

        disconnectFromVoiceChannel(m_connected_to_voice_channel);
        m_connected_to_voice_channel = SleepyDiscord::Snowflake<SleepyDiscord::Channel>();
        m_voice_event_handler = nullptr; //SleepyDiscord will delete this.
    }

    void DiscordClient::onServer(SleepyDiscord::Server server)
    {
        BaseDiscordClient::onServer(server);
    }

    bool DiscordClient::has_permission(SleepyDiscord::Channel channel, SleepyDiscord::Permission permission)
    {
        SleepyDiscord::Server server = get_server(channel.serverID);

        auto it = std::find_if(server.members.begin(), server.members.end(), [this](const SleepyDiscord::ServerMember& member)
        {
            return member.user == m_current_user;
        });

        int64_t permissions;
        if (it != server.members.end())
        {
            auto server_member = *it;
            for (const auto& role_id : server_member.roles)
            {
                auto role = std::find_if(server.roles.begin(), server.roles.end(), [role_id](const SleepyDiscord::Role& role){ return role_id == role.ID; });
                if (role != server.roles.end())
                {
                    permissions |= role->permissions;
                }
            }

            auto everyone_role = std::find_if(server.roles.begin(), server.roles.end(), [](const SleepyDiscord::Role& role){ return role.name == "@everyone"; });
            if (everyone_role != server.roles.end())
            {
                permissions |= everyone_role->permissions;
            }

            for (const auto& override : channel.permissionOverwrites)
            {
                if (override.type == "member")
                {
                    if (override.ID.number() == m_current_user.ID.number())
                    {
                        permissions |= override.allow;
                        permissions &= ~override.deny;
                    }
                }
                else if (override.type == "role")
                {
                    auto it = std::find_if(server_member.roles.begin(), server_member.roles.end(), [override](const SleepyDiscord::Snowflake<SleepyDiscord::Role>& role){ return role.number() == override.ID.number(); });
                    if (it != server_member.roles.end() || override.ID.number() == everyone_role->ID.number())
                    {
                        permissions |= override.allow;
                        permissions &= ~override.deny;
                    }
                }
                else
                {
                    g_warning("DiscordClient::has_permission - Unhandled permission overwrite type %s", override.type.c_str());
                }
            }
        }

        bool has_permission =  (permissions & permission) != 0;
        return has_permission;
    }
}