#include "pch.h"

namespace discord
{
    class MessageWidgetData : public Glib::Object
    {
    public:
        static Glib::RefPtr<MessageWidgetData> create(const SleepyDiscord::Message& message)
        {
            return Glib::make_refptr_for_instance<MessageWidgetData>(new MessageWidgetData(message));
        }

        static Glib::RefPtr<MessageWidgetData> create()
        {
            return Glib::make_refptr_for_instance<MessageWidgetData>(new MessageWidgetData());
        }

        const SleepyDiscord::Message& get_message() const { return m_message; }
        void set_message(const SleepyDiscord::Message& message) { m_message = message; }

        const SleepyDiscord::Attachment& get_display_attachment() const { return m_display_attachment; }
        void set_display_attachment(const SleepyDiscord::Attachment& display_attachment) { m_display_attachment = display_attachment; }

        bool get_show_name() const { return m_show_name; }
        void set_show_name(bool show_name) { m_show_name = show_name; }

        bool get_hide_text() const { return m_hide_text; }
        void set_hide_text(bool hide_text) { m_hide_text = hide_text; }

        Glib::PropertyProxy<std::string> property_image_url() { return m_image_url.get_proxy(); }

        bool get_loading_placeholder() const { return m_placeholder; }
        void set_loading_placeholder(bool placeholder) { m_placeholder = placeholder; }

    private:
        MessageWidgetData()
                : Glib::ObjectBase(typeid(MessageWidgetData))
                , m_image_url(*this, "image_url")
                , m_placeholder(true)
        {
        }

        explicit MessageWidgetData(const SleepyDiscord::Message& message)
                : Glib::ObjectBase(typeid(MessageWidgetData))
                , m_message(message)
                , m_image_url(*this, "image_url")
        {
        }

        SleepyDiscord::Message m_message;
        SleepyDiscord::Attachment m_display_attachment;
        bool m_show_name = true;
        bool m_hide_text = false;
        Glib::Property<std::string> m_image_url;
        bool m_placeholder = false;
    };
}
